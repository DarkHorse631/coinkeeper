package inc.brocorp.controller;

import inc.brocorp.model.ResponseError;
import inc.brocorp.model.TokenDto;
import inc.brocorp.model.UserAuthDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/api/auth")
@Api(value = "API для аутентификации пользователя")
public interface AuthController {

    @ApiOperation(value = "Аутентификации пользователя по логину и паролю")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользователя успешно аутентифицирован",
                    response = TokenDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена аутентификация", response = ResponseError.class)
    })
    @GetMapping
    ResponseEntity<TokenDto> authenticate(@RequestBody UserAuthDto userAuthDto);
}
