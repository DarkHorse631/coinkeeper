package inc.brocorp.controller;

import inc.brocorp.model.ResponseError;
import inc.brocorp.model.UserDto;
import inc.brocorp.model.UserIdentificationDto;
import inc.brocorp.model.UserPrincipalDto;
import inc.brocorp.model.search.PageDto;
import inc.brocorp.model.search.Search;
import inc.brocorp.model.search.UserSearchDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

@RequestMapping("/api/v1/users")
@Api(value = "API для работы с пользователем")
public interface UserController {



    @ApiOperation(value = "Создание аккаунта пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Аккаунт пользователя успешно создан",
                    response = UserDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @PostMapping
    ResponseEntity<UserDto> create(@RequestBody UserIdentificationDto user, UriComponentsBuilder componentsBuilder);


    @ApiOperation(value = "Получение детальной информации пользователя по его id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Аккаунт пользователя успешно найден",
                    response = UserDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @ApiParam(name = "id", required = true)
    @GetMapping("/{id}")
    ResponseEntity<UserDto> findById(@ApiParam(value = "Id пользователя", required = true) @PathVariable UUID id);

    @GetMapping
    @ApiOperation(value = "Получение списка всех пользователей, с возможностью указать параметры поиска")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Список найденных пользователей",
                    response = UserDto.class, responseContainer = "PageDto"),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    ResponseEntity<PageDto<UserDto>> findAll(@RequestBody Search<UserSearchDto> userSearch);

    @ApiOperation(value = "Обновление данных пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Данные пользователя успешно обновлены",
                    response = UserDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @PutMapping("/{id}")
    @ApiParam(name = "id", required = true)
    ResponseEntity<UserDto> update(@ApiParam(value = "Id пользователя", required = true) @PathVariable UUID id, @RequestBody UserDto user);

    @ApiOperation(value = "Удаление аккаунта пользователя")
    @ApiParam(name = "id", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Аккаунт пользователя успешно удален"),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@ApiParam(value = "Id пользователя", required = true) @PathVariable UUID id);

    @ApiOperation(value = "Получение информации идентификации пользователя ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Данные идентификации пользователя успешно найден",
                    response = UserDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @GetMapping("/info")
    ResponseEntity<UserPrincipalDto> getInfo();
}
