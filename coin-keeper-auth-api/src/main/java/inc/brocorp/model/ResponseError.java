package inc.brocorp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.UUID;

@ApiModel(value = "Базовая модель ошибки при выполнении запроса")
public class ResponseError implements Serializable {

    @ApiModelProperty(value = "Идентификатор ошибки", example = "6e328c1a-2e99-4635-9668-b1a437d1db18", allowEmptyValue = true)
    private UUID id;

    @ApiModelProperty(value = "Сообщение ошибки", example = "Password doesn't match")
    private String message;

    @ApiModelProperty(value = "Код ошибки", example = "IllegalArgumentException")
    private String codeError;

    public ResponseError(UUID id, String message, String codeError) {
        this.id = id;
        this.message = message;
        this.codeError = codeError;
    }

    public ResponseError() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCodeError() {
        return codeError;
    }

    public void setCodeError(String codeError) {
        this.codeError = codeError;
    }

    @Override
    public String toString() {
        return "ResponseError{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", codeError='" + codeError + '\'' +
                '}';
    }
}
