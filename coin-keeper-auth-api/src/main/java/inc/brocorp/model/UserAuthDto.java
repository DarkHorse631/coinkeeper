package inc.brocorp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Данные пользователя для аутентификации")
public class UserAuthDto {

    @ApiModelProperty(value = "Имя пользователя", example = "user1", required = true)
    private String username;

    @ApiModelProperty(value = "Пароль пользователя", example = "pass1", required = true)
    private String password;

    @ApiModelProperty(value = "Система для которой будет предназначен токен", example = "coin-keeper-impl", required = true)
    private String audience;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    @Override
    public String toString() {
        return "UserAuthDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", audience='" + audience + '\'' +
                '}';
    }
}
