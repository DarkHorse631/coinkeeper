package inc.brocorp.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value ="Описание дополнительных полей необходимых для последующей аутентификации пользователя")
public class UserIdentificationDto extends UserDto implements Serializable {

    @ApiModelProperty(value = "Пароль пользователя", example = "passw@rd1", required = true)
    private String password;

    @ApiModelProperty(value = "Повторный пароль", example = "passw@rd1", required = true)
    private String repeatPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    @Override
    public String toString() {
        return "UserIdentificationDto{" +
                "password='" + password + '\'' +
                ", repeatPassword='" + repeatPassword + '\'' +
                '}';
    }
}
