package inc.brocorp.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

public class UserPrincipalDto {

    @ApiModelProperty(value = "Имя пользователя", example = "user1", required = true)
    private final String username;
    @ApiModelProperty(value = "Аккаунт не просрочен?", example = "true", required = true)
    private final boolean isAccountNonExpired;
    @ApiModelProperty(value = "Аккаунт не заблокирован?", example = "true", required = true)
    private final boolean isAccountNonLocked;
    @ApiModelProperty(value = "", example = "true", required = true)
    private final boolean isCredentialsNonExpired;
    @ApiModelProperty(value = "Пользователь Активен?", example = "true", required = true)
    private final boolean isEnabled;
    @ApiModelProperty(value = "Токен пользователя", example = "xxx.yyy.zzz", required = true)
    private final String token;
    @ApiModelProperty(value = "Пользователь аутентифицирован", example = "true", required = true)
    private final boolean authenticated;
    @ApiModelProperty(value = "Роли пользователя", required = true)
    private final List<Object> authorities;

    public UserPrincipalDto(String username, boolean isAccountNonExpired, boolean isAccountNonLocked,
                       boolean isCredentialsNonExpired, boolean isEnabled, String token,
                       boolean authenticated, List<Object> authorities) {
        this.username = username;
        this.isAccountNonExpired = isAccountNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;
        this.token = token;
        this.authenticated = authenticated;
        this.authorities = authorities;
    }

    public String getUsername() {
        return username;
    }

    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public String getToken() {
        return token;
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public List<Object> getAuthorities() {
        return authorities;
    }

    public static UserPrincipalBuilder builder() {
        return new UserPrincipalBuilder();
    }

    public static class UserPrincipalBuilder {

        private String username;
        private boolean isAccountNonExpired;
        private boolean isAccountNonLocked;
        private boolean isCredentialsNonExpired;
        private boolean isEnabled;
        private String token;
        private boolean authenticated;
        private List<Object> authorities = new ArrayList<>();

        public UserPrincipalBuilder setUsername(String username) {
            this.username = username;
            return this;
        }

        public UserPrincipalBuilder setAccountNonExpired(boolean accountNonExpired) {
            isAccountNonExpired = accountNonExpired;
            return this;
        }

        public UserPrincipalBuilder setAccountNonLocked(boolean accountNonLocked) {
            isAccountNonLocked = accountNonLocked;
            return this;
        }

        public UserPrincipalBuilder setCredentialsNonExpired(boolean credentialsNonExpired) {
            isCredentialsNonExpired = credentialsNonExpired;
            return this;
        }

        public UserPrincipalBuilder setEnabled(boolean enabled) {
            isEnabled = enabled;
            return this;
        }

        public UserPrincipalBuilder setToken(String token) {
            this.token = token;
            return this;
        }

        public UserPrincipalBuilder setAuthenticated(boolean authenticated) {
            this.authenticated = authenticated;
            return this;
        }

        public UserPrincipalBuilder setAuthorities(List<Object> authorities) {
            this.authorities = authorities;
            return this;
        }

        public UserPrincipalDto build() {
            return new UserPrincipalDto(this.username, this.isAccountNonExpired, this.isAccountNonLocked,
                    this.isCredentialsNonExpired, this.isEnabled, this.token, this.authenticated, this.authorities);
        }
    }
}
