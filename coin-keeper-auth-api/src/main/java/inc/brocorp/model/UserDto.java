package inc.brocorp.model;


import inc.brocorp.enums.Role;
import inc.brocorp.enums.UserStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@ApiModel(value = "Базовая модель пользователя")
public class UserDto implements Serializable {

    @ApiModelProperty(value = "Идентификатор пользователя", example = "6e328c1a-2e99-4635-9668-b1a437d1db18", allowEmptyValue = true)
    private UUID id;

    @ApiModelProperty(value = "Имя пользователя", example = "user1", required = true)
    private String username;

    @ApiModelProperty(value = "Почта пользователя", example = "email@gmail.com", required = true)
    private String email;

    @ApiModelProperty(value = "Дата создания", example = "2021-02-02T02:26:47", allowEmptyValue = true)
    private LocalDateTime createAt;

    @ApiModelProperty(value = "Дата обновления", example = "2021-02-03T06:21:57", allowEmptyValue = true)
    private LocalDateTime updateAt;

    @ApiModelProperty(value = "Роль пользователя", example = "USER", required = true)
    private Role role;

    @ApiModelProperty(value = "Статус пользователя", example = "DRAFT", allowableValues = "DRAFT, ENABLED, DISABLED, BLOCKED")
    private UserStatus status;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public LocalDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(LocalDateTime updateAt) {
        this.updateAt = updateAt;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public String getStatusDescription(){
        if (this.status == null) return null;
        return  status.getDescription();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", createAt=" + createAt +
                ", updateAt=" + updateAt +
                ", role=" + role +
                ", status=" + status +
                '}';
    }
}
