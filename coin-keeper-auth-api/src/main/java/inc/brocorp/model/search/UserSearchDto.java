package inc.brocorp.model.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Базовая модель  для поиска пользователей")
public class UserSearchDto {

    @ApiModelProperty(value = "Имя пользователя", example = "user1", allowEmptyValue = true)
    String username;

    @ApiModelProperty(value = "Статус пользователя", example = "DRAFT", allowEmptyValue = true)
    String status;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
