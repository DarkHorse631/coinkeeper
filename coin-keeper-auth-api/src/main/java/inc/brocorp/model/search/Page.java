package inc.brocorp.model.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Базовая модель для пагинации")
public class Page {

    @ApiModelProperty(value = "Страница в списке", example = "0", required = true)
    int page;
    @ApiModelProperty(value = "Кол-во записей на странице", example = "5", required = true)
    int size;

    public Page(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public Page() {
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}

