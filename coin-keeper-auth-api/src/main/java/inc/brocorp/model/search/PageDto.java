package inc.brocorp.model.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(value = "Базовая модель содержащая результаты поиска")
public class PageDto<T> {

    @ApiModelProperty(value = "Список объектов подошедших под критерии поиска")
    List<T> data;

    @ApiModelProperty(value = "Общее кол-во записей в БД подходящих под критерии запроса")
    long total;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }
}
