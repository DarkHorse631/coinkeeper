package inc.brocorp.enums;

public enum UserStatus {
    DRAFT("Черновик"),
    ENABLED("Активирован"),
    DISABLED("Деактивирован"),
    BLOCKED("Заблокирован");

    private String description;

    UserStatus(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
