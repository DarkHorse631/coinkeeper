package inc.brocorp.enums;

public enum Role {

    ADMIN("Администратор"),
    USER("Пользователь"),
    ;

    private String description;

    Role(String description) {
        this.description = description;
    }
}
