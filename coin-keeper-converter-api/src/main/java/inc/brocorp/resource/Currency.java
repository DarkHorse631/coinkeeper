package inc.brocorp.resource;

import java.math.BigDecimal;

/**
 * Список валют доступных для конвертации
 */
public enum Currency {
    USD(new BigDecimal("75.91")),
    EUR(new BigDecimal("88.55"));

    /**
     * Отношение одной единицы валюты к рублю
     */
    private BigDecimal value;

    public BigDecimal getValue() {
        return value;
    }

    Currency(BigDecimal value) {
        this.value = value;
    }
}
