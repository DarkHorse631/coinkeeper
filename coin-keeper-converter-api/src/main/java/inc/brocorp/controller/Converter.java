package inc.brocorp.controller;

import inc.brocorp.resource.Currency;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

@RequestMapping("api/currencies")
@Api(value = "API для перевода рублей в определенную валюту")
public interface Converter {

    @ApiOperation(value = "Перевод рублей в определенную валюту")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Сумма успешно конвертирована"),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка")
    })
    @GetMapping("/{currency}")
    ResponseEntity<BigDecimal> convert(@PathVariable String currency, @RequestParam String sum);

    @ApiOperation(value = "Получение списка валют в которые можно конвертировать рубли")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Список успешно получен")
    })
    @GetMapping
    ResponseEntity<List<Currency>> getCurrencies();
}
