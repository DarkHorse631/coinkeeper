package inc.brocorp.model;

import io.jsonwebtoken.impl.DefaultClaims;

import java.util.Map;

public class JwtClaims extends DefaultClaims {

    private static final String EMAIL = "email";
    private static final String ROLE = "role";

    public JwtClaims() {
    }

    public JwtClaims(Map<String, Object> map) {
        super(map);
    }

    public String getRole() {
        return getString(ROLE);
    }

    public JwtClaims setRole(String role) {
        setValue(ROLE, role);
        return this;
    }

    public String getEmail() {
        return getString(EMAIL);
    }

    public JwtClaims setEmail(String email) {
        setValue(EMAIL, email);
        return this;
    }
}
