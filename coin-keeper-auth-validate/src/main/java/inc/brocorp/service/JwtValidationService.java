package inc.brocorp.service;

import inc.brocorp.model.JwtClaims;

public interface JwtValidationService {

    void validateJwtClaims(JwtClaims claims, String applicationName);

    JwtClaims getClaimsFromJwt(String token);
}
