package inc.brocorp.service;

import inc.brocorp.model.JwtClaims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.crypto.spec.SecretKeySpec;
import java.util.Date;

@Component
public class JwtValidationServiceImpl implements JwtValidationService {

    @Value("${security.secret-key}")
    private String secretKey;

    @Override
    public void validateJwtClaims(JwtClaims claims, String applicationName) {
        if(ObjectUtils.isEmpty(applicationName)){
            throw new IllegalArgumentException("Invalid application name");
        }
        if (new Date(System.currentTimeMillis()).after(claims.getExpiration())) {
            throw new BadCredentialsException("Expired token");
        }
        if (ObjectUtils.isEmpty(claims.getId()) || ObjectUtils.isEmpty(claims.getSubject())
                || ObjectUtils.isEmpty(claims.getAudience()) || ObjectUtils.isEmpty(claims.getIssuer())) {
            throw new BadCredentialsException("Invalid token payload");
        }
        if (ObjectUtils.isEmpty(claims.getRole())) {
            throw new BadCredentialsException("Invalid token role claim");
        }
        if (ObjectUtils.isEmpty(claims.getEmail())) {
            throw new BadCredentialsException("Invalid token email claim");
        }
        if (!applicationName.equals(claims.getAudience())) {
            throw new BadCredentialsException("Invalid token audience claim");
        }
    }

    @Override
    public JwtClaims getClaimsFromJwt(String token) {
        if(ObjectUtils.isEmpty(token)){
            throw new IllegalArgumentException("Invalid token");
        }
        if(ObjectUtils.isEmpty(secretKey)){
            throw new IllegalArgumentException("Invalid secret key");
        }
        try {
            return new JwtClaims(Jwts.parserBuilder()
                    .setSigningKey(new SecretKeySpec(Decoders.BASE64.decode(secretKey), SignatureAlgorithm.HS256.getJcaName()))
                    .build()
                    .parseClaimsJws(token)
                    .getBody());
        } catch (SignatureException | MalformedJwtException | UnsupportedJwtException | IllegalArgumentException e) {
            throw new BadCredentialsException("Invalid token", e);
        }
    }
}
