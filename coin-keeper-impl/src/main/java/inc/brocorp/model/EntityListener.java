package inc.brocorp.model;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

/**
 * Класс выполняющий простановку дат создания и обновления сущностей
 */
public class EntityListener{

    @PrePersist
    public void beforeCreate(AbstractEntity entity){
        entity.setCreateAt(LocalDateTime.now());
        entity.setUpdateAt(LocalDateTime.now());
    }

    @PreUpdate
    public void beforeUpdate(AbstractEntity entity){
        entity.setUpdateAt(LocalDateTime.now());
    }
}
