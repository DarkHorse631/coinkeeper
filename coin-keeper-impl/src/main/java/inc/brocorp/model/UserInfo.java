package inc.brocorp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "users_info", schema = "coin_keeper")
public class UserInfo {

    @Id
    @Column(name = "user_id")
    private UUID userId;

    public UserInfo() {
    }

    public UserInfo(UUID userId) {
        this.userId = userId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}
