package inc.brocorp.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Абстарктный класс содержащий поля, необходимы для каждой сущности
 */
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    /**
     * Иденитфикатор сущности
     */
    @Id
    protected UUID id;

    /**
     * Дата создания
     */
    @Column(name = "create_at")
    protected LocalDateTime createAt;

    /**
     * Дата обновления
     */
    @Column(name = "update_at")
    protected LocalDateTime updateAt;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public LocalDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(LocalDateTime updateAt) {
        this.updateAt = updateAt;
    }
}
