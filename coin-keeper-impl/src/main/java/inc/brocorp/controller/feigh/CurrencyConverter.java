package inc.brocorp.controller.feigh;

import inc.brocorp.controller.Converter;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "converter", url = "${feign.converter.url}")
public interface CurrencyConverter extends Converter {
}
