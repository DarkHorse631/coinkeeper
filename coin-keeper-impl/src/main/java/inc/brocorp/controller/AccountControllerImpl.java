package inc.brocorp.controller;


import inc.brocorp.annotation.Audit;
import inc.brocorp.audit.AuditAction;
import inc.brocorp.resource.controller.AccountController;
import inc.brocorp.resource.dto.AccountDto;
import inc.brocorp.resource.dto.search.AccountSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import inc.brocorp.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Objects;
import java.util.UUID;

@RestController
public class AccountControllerImpl implements AccountController {

    private final AccountService accountService;
    private final static Logger log = LoggerFactory.getLogger(AccountControllerImpl.class);

    public AccountControllerImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    @Audit(action = AuditAction.ACCOUNT_CREATE)
    public ResponseEntity<AccountDto> create(@RequestBody AccountDto account, UriComponentsBuilder componentsBuilder) {
        log.debug("Method 'create' start with {}", account);
        AccountDto accountDto = accountService.create(account);
        URI uri = componentsBuilder.path("/api/accounts/" + accountDto.getId()).buildAndExpand(accountDto).toUri();
        log.debug("Method 'create' end with {}", accountDto);
        return ResponseEntity.created(uri).body(accountDto);
    }

    @Override
    public ResponseEntity<AccountDto> findById(@PathVariable UUID id) {
        log.debug("Method 'findById' start with id {}", id);
        AccountDto accountDto = accountService.findById(id);
        log.debug("Method 'findById' end with {}", accountDto);
        return new ResponseEntity<>(accountDto, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PageDto<AccountDto>> findAll(@RequestBody Search<AccountSearchDto> accountSearchDto) {
        log.debug("Method 'findAll' start with {}", accountSearchDto);
        PageDto<AccountDto> accounts = accountService.getAccounts(accountSearchDto);
        log.debug("Method 'findAll' find {} records", accounts.getTotal());
        return new ResponseEntity<>(accounts, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    @Audit(action = AuditAction.ACCOUNT_UPDATE)
    public ResponseEntity<AccountDto> update(@PathVariable UUID id, @RequestBody AccountDto account) {
        log.debug("Method 'update' start with id {} and {}", id, account);
        if (!Objects.equals(id, account.getId()))
            throw new IllegalArgumentException("Id = " + account.getId() + " Expected same as : " + id);
        AccountDto accountDto = accountService.update(account);
        log.debug("Method 'update' end with {}", accountDto);
        return new ResponseEntity<>(accountDto, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    @Audit(action = AuditAction.ACCOUNT_DELETE)
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        log.debug("Method 'delete' start with id {}", id);
        accountService.delete(id);
        log.debug("Method 'update' with id {} ended", id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
}
