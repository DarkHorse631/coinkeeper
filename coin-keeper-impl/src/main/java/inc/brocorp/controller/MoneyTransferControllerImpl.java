package inc.brocorp.controller;

import inc.brocorp.annotation.Audit;
import inc.brocorp.audit.AuditAction;
import inc.brocorp.resource.controller.MoneyTransferController;
import inc.brocorp.resource.dto.MoneyTransferDto;
import inc.brocorp.resource.dto.search.MoneyTransferSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import inc.brocorp.service.MoneyTransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Objects;
import java.util.UUID;

@RestController
public class MoneyTransferControllerImpl implements MoneyTransferController {

    private final MoneyTransferService moneyTransferService;
    private final static Logger log = LoggerFactory.getLogger(MoneyTransferControllerImpl.class);

    public MoneyTransferControllerImpl(MoneyTransferService moneyTransferService) {
        this.moneyTransferService = moneyTransferService;
    }

    @Override
    @Audit(action = AuditAction.TRANSFER_CREATE)
    public ResponseEntity<MoneyTransferDto> create(@RequestBody MoneyTransferDto moneyTransfer, UriComponentsBuilder componentsBuilder) {
        log.debug("Method 'create' start with {}", moneyTransfer);
        MoneyTransferDto moneyTransferDto = moneyTransferService.create(moneyTransfer);
        URI uri = componentsBuilder.path("/api/money_transfers/" + moneyTransferDto.getId()).buildAndExpand(moneyTransferDto).toUri();
        log.debug("Method 'create' end with {}", moneyTransferDto);
        return ResponseEntity.created(uri).body(moneyTransferDto);
    }

    @Override
    public ResponseEntity<MoneyTransferDto> findById(@PathVariable UUID id) {
        log.debug("Method 'findById' start with id {}", id);
        MoneyTransferDto moneyTransferDto = moneyTransferService.findById(id);
        log.debug("Method 'findById' end with {}", moneyTransferDto);
        return new ResponseEntity<>(moneyTransferDto, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PageDto<MoneyTransferDto>> findAll(@RequestBody Search<MoneyTransferSearchDto> moneyTransferSearchDto) {
        log.debug("Method 'findAll' start with {}", moneyTransferSearchDto);
        PageDto<MoneyTransferDto> moneyTransferDtoList = moneyTransferService.getTransfers(moneyTransferSearchDto);
        log.debug("Method 'findAll' find {} records", moneyTransferDtoList.getTotal());
        return new ResponseEntity<>(moneyTransferDtoList, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    @Audit(action = AuditAction.TRANSFER_UPDATE)
    public ResponseEntity<MoneyTransferDto> update(@PathVariable UUID id, @RequestBody MoneyTransferDto moneyTransfer) {
        log.debug("Method 'update' start with id {} and {}", id, moneyTransfer);
        if (!Objects.equals(id, moneyTransfer.getId()))
            throw new IllegalArgumentException("Id = " + moneyTransfer.getId() + " Expected same as : " + id);
        MoneyTransferDto moneyTransferDto = moneyTransferService.update(moneyTransfer);
        log.debug("Method 'update' end with {}", moneyTransferDto);
        return new ResponseEntity<>(moneyTransferDto, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    @Audit(action = AuditAction.TRANSFER_DELETE)
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        log.debug("Method 'delete' start with id {}", id);
        moneyTransferService.delete(id);
        log.debug("Method 'update' with id {} ended", id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }
}
