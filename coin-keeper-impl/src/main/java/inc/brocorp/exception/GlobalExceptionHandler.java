package inc.brocorp.exception;


import inc.brocorp.resource.dto.ResponseError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.UUID;

@RestControllerAdvice(basePackages = "inc.brocorp.controller")
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ResponseError> handleIllegalArgumentException(IllegalArgumentException e) {
        final ResponseError responseError = new ResponseError(UUID.randomUUID(), e.getMessage(), e.getClass().getSimpleName());
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ResponseError> handleRuntimeException(RuntimeException e) {
        final ResponseError responseError = new ResponseError(UUID.randomUUID(), e.getMessage(), e.getClass().getSimpleName());
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityException.class)
    public ResponseEntity<ResponseError> handleEntityException(EntityException e){
        final ResponseError responseError = new ResponseError(UUID.randomUUID(), e.getMessage(), e.getClass().getSimpleName());
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

//    @ExceptionHandler(AuthenticationServiceException.class)
//    public ResponseEntity<ResponseError> handleAuthenticationException(AuthenticationServiceException e){
//        final ResponseError responseError = new ResponseError(UUID.randomUUID(), e.getMessage(), e.getClass().getSimpleName());
//        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
//    }

    @ExceptionHandler(AuthenticationServiceException.class)
    public ResponseEntity<ResponseError> handleAccessDeniedException(AuthenticationServiceException e){
        final ResponseError responseError = new ResponseError(UUID.randomUUID(), e.getMessage(), e.getClass().getSimpleName());
        return new ResponseEntity<>(responseError, new HttpHeaders(), HttpStatus.FORBIDDEN);
    }
}
