package inc.brocorp.jms;

import inc.brocorp.dto.UserInfoDto;
import inc.brocorp.enums.MessageStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class JMSSender {

    private final static Logger log = LoggerFactory.getLogger(JMSSender.class);
    private final JmsTemplate jmsTemplate;


    public JMSSender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Transactional
    public void sendMessage(final String queueName, final UserInfoDto userInfoDto) {
        userInfoDto.setMessageStatus(MessageStatus.SUCCESS);
        jmsTemplate.setTimeToLive(30000);
        jmsTemplate.convertAndSend(queueName, userInfoDto);
        log.info("Sending user info response {} to queue - {}", userInfoDto, queueName);
    }


}
