package inc.brocorp.jms;


import inc.brocorp.dto.UserInfoDto;
import inc.brocorp.model.UserInfo;
import inc.brocorp.repo.UserInfoRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Component
public class MessageListener {

    @Value("${inc.brocorp.queue.user-id-response}")
    private String queueName;
    private final static Logger log = LoggerFactory.getLogger(MessageListener.class);
    private final UserInfoRepo userInfoRepo;
    private final JMSSender jmsSender;

    public MessageListener(UserInfoRepo userInfoRepo, JMSSender jmsSender) {
        this.userInfoRepo = userInfoRepo;
        this.jmsSender = jmsSender;
    }

    @JmsListener(destination = "${inc.brocorp.queue.user-id-send}")
    @Transactional
    public void receiveMessage(final UserInfoDto message) {
        log.info("Received message from Auth-service{} ", message.toString());
        UserInfo userInfo = new UserInfo(message.getUserId());
        userInfoRepo.save(userInfo);
        jmsSender.sendMessage(queueName, message);
    }

}
