package inc.brocorp.service;


import inc.brocorp.resource.dto.MoneyTransferDto;
import inc.brocorp.resource.dto.search.MoneyTransferSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.UUID;

/**
 * Класс содержащий API для работы с денежными переводами
 */
public interface MoneyTransferService {

    /**
     * Метод для создания денежных переводов
     *
     * @param moneyTransfer - перевод, данные которого будут сохранены
     * @return - данные созданного перевода
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    MoneyTransferDto create(MoneyTransferDto moneyTransfer);

    /**
     * Метод для поиска денежного перевода по его id
     *
     * @param id - по которому будет происходить поиск
     * @return данные денежного перевода
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    MoneyTransferDto findById(UUID id);

    /**
     * Метод для поиска всех денежных переводов
     *
     * @param accountId - id счета для которого необходимо найте перводы
     * @return список всех денежных переводов на счете
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    List<MoneyTransferDto> findAllByAccountId(UUID accountId);

    /**
     * Метод для поиска переводов с учетом пагинации и набором параметров для поиска
     *
     * @param moneyTransferSearchDto - объект содержащий критерии поиска
     * @return - список преводов подходящих по условия поиска
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    PageDto<MoneyTransferDto> getTransfers(Search<MoneyTransferSearchDto> moneyTransferSearchDto);

    /**
     * Метод для обновления данных денежного перевода
     *
     * @param moneyTransfer - новые данные перевода
     * @return обновленные данные
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    MoneyTransferDto update(MoneyTransferDto moneyTransfer);

    /**
     * Метод для удаления денежного перевода
     *
     * @param id - по которому необходимо удалить перевод
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    void delete(UUID id);

    /**
     * Метод, проверяющий есть ли данный счет в БД приложения
     * @param id - эелектронного счета, который необходимо проверить
     * @return - true, если есть в БД приложения
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    boolean isExist(UUID id);
}
