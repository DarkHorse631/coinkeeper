package inc.brocorp.service;


import inc.brocorp.resource.dto.AccountDto;
import inc.brocorp.resource.dto.search.AccountSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import org.springframework.security.access.prepost.PreAuthorize;

import java.math.BigDecimal;
import java.util.UUID;

/**
 * Класс содержащий API для работы с эелектронными счетами
 */
public interface AccountService {

    /**
     * Метод для создания электронного счета
     *
     * @param account - электронный счет, данные которого будут сохранены
     * @return - данные созданного счета
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    AccountDto create(AccountDto account);

    /**
     * Метод для поиска электронного счета по его id
     *
     * @param id - по которому будет происходить поиск
     * @return данные электронного счета
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    AccountDto findById(UUID id);

    /**
     * Метод для поиска счетов с учетом пагинации и набором параметров для поиска
     *
     * @param accountSearchDto - объект содержащий критерии поиска
     * @return - список счетов подходящих по условия поиска
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    PageDto<AccountDto> getAccounts(Search<AccountSearchDto> accountSearchDto);

    /**
     * Метод для обновления данных об электронном счете
     *
     * @param account - электронный счет, содержащий новые данные
     * @return обновленные данные
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    AccountDto update(AccountDto account);

    /**
     * Позволяет изменить сумму счета
     *
     * @param id           - идентификатор счета сумму которого необходимо поменять
     * @param amount       - сумма добавляемая или убавляемая к счету
     * @param isDecreasing - если true то сумму необходимо отнять от сыммы счета
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    void updateTotalSum(UUID id, BigDecimal amount, boolean isDecreasing);

    /**
     * Метод для удаления электронного счета
     *
     * @param id - по которому необходимо удалить счет
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    void delete(UUID id);

    /**
     * Метод, проверяющий есть ли данный счет в БД приложения
     *
     * @param id - эелектронного счета, который необходимо проверить
     * @return - true, если есть в БД приложения
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    boolean isExist(UUID id);
}
