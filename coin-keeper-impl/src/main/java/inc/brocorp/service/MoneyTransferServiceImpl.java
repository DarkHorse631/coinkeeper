package inc.brocorp.service;


import inc.brocorp.exception.EntityException;
import inc.brocorp.model.Account;
import inc.brocorp.model.MoneyTransfer;
import inc.brocorp.repo.MoneyTransferRepo;
import inc.brocorp.resource.dto.MoneyTransferDto;
import inc.brocorp.resource.dto.search.MoneyTransferSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class MoneyTransferServiceImpl implements MoneyTransferService {

    private final MoneyTransferRepo moneyTransferRepo;
    private final AccountService accountService;
    private static Logger log = Logger.getLogger(AccountServiceImpl.class.getName());

    @Value("${exception.doesNotExist}")
    private String exceptionDoesNotExist;

    public MoneyTransferServiceImpl(MoneyTransferRepo moneyTransferRepo, AccountService accountService) {
        this.moneyTransferRepo = moneyTransferRepo;
        this.accountService = accountService;
    }

    @Transactional
    public MoneyTransferDto create(MoneyTransferDto moneyTransferDto) {
        moneyTransferDto.setId(UUID.randomUUID());
        if (!accountService.isExist(moneyTransferDto.getAccountId()))
            throw new EntityException(exceptionDoesNotExist);
        MoneyTransfer moneyTransfer = dtoToModel(moneyTransferDto);
        accountService.updateTotalSum(moneyTransfer.getAccount().getId(), moneyTransfer.getAmount(), false);
        return modelToDto(moneyTransferRepo.save(moneyTransfer));
    }

    @Transactional(readOnly = true)
    public MoneyTransferDto findById(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        if (!moneyTransferRepo.existsById(id)) throw new EntityException(exceptionDoesNotExist);
        return modelToDto(moneyTransferRepo.findById(id).get());
    }

    @Transactional(readOnly = true)
    public List<MoneyTransferDto> findAllByAccountId(UUID accountId) {
        if (ObjectUtils.isEmpty(accountId)) throw new IllegalArgumentException("Id is empty.");
        final List<MoneyTransferDto> result = new ArrayList<>();
        for (MoneyTransfer c : moneyTransferRepo.findAllByAccount_Id(accountId)) {
            result.add(modelToDto(c));
        }
        return result;
    }

    @Transactional
    public MoneyTransferDto update(MoneyTransferDto moneyTransferDto) {
        if (ObjectUtils.isEmpty(moneyTransferDto.getId())) throw new IllegalArgumentException("Id is empty.");
        if (!moneyTransferRepo.existsById(moneyTransferDto.getId())) throw new EntityException(exceptionDoesNotExist);
        MoneyTransfer moneyTransfer = dtoToModel(moneyTransferDto);
        return modelToDto(moneyTransferRepo.save(moneyTransfer));
    }

    @Transactional
    public void delete(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        MoneyTransfer moneyTransfer = moneyTransferRepo.findById(id).orElse(null);
        if (moneyTransfer == null) throw new EntityException(exceptionDoesNotExist);
        accountService.updateTotalSum(moneyTransfer.getAccount().getId(), moneyTransfer.getAmount(), true);
        moneyTransferRepo.deleteById(id);
    }

    @Transactional(readOnly = true)
    public boolean isExist(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        return moneyTransferRepo.existsById(id);
    }

    @Async
    void logInfo(String operationType, MoneyTransferDto moneyTransferDto) {
        log.info("Operation type: " + operationType + ", result: " + moneyTransferDto.toString());
    }

    @Transactional(readOnly = true)
    public PageDto<MoneyTransferDto> getTransfers(Search<MoneyTransferSearchDto> moneyTransferSearchDto) {
        Page<MoneyTransfer> page = moneyTransferRepo
                .findAll(getSpec(moneyTransferSearchDto.getData()), getOf(moneyTransferSearchDto));
        var transfers = page
                .map(this::modelToDto).toList();
        final PageDto<MoneyTransferDto> pageDto = new PageDto<>();
        pageDto.setData(transfers);
        pageDto.setTotal(page.getTotalElements());
        return pageDto;
    }

    private PageRequest getOf(Search<MoneyTransferSearchDto> moneyTransferSearchDto) {
        var page = moneyTransferSearchDto.getPage();
        return PageRequest.of(page.getPage(), page.getSize());
    }

    private Specification<MoneyTransfer> getSpec(MoneyTransferSearchDto moneyTransferSearchDto) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (!ObjectUtils.isEmpty(moneyTransferSearchDto.getAccountId())) {
                Join<MoneyTransfer, Account> account = root.join("account");
                predicates.add(builder.equal(account.get("id"), moneyTransferSearchDto.getAccountId()));
            }
            if (!ObjectUtils.isEmpty(moneyTransferSearchDto.getTitle())) {
                predicates.add(builder.like(root.get("title"), moneyTransferSearchDto.getTitle()));
            }
            if (!ObjectUtils.isEmpty(moneyTransferSearchDto.getCreateAt())) {
                predicates.add(builder.equal(root.get("create_at"), moneyTransferSearchDto.getCreateAt()));
            }
            return builder.and(predicates.toArray(Predicate[]::new));
        };
    }

    public MoneyTransfer dtoToModel(MoneyTransferDto transferDto) {
        final MoneyTransfer mt = new MoneyTransfer();
        final Account account = new Account();
        account.setId(transferDto.getAccountId());
        mt.setId(transferDto.getId());
        mt.setTitle(transferDto.getTitle());
        mt.setAccount(account);
        mt.setAmount(new BigDecimal(transferDto.getAmount()));
        mt.setCreateAt(transferDto.getCreateAt());
        mt.setUpdateAt(transferDto.getUpdateAt());
        return mt;
    }

    public MoneyTransferDto modelToDto(MoneyTransfer transfer) {
        final MoneyTransferDto mt = new MoneyTransferDto();
        mt.setId(transfer.getId());
        mt.setTitle(transfer.getTitle());
        mt.setAccountId(transfer.getAccount().getId());
        mt.setAmount(transfer.getAmount().toString());
        mt.setCreateAt(transfer.getCreateAt());
        mt.setUpdateAt(transfer.getUpdateAt());
        return mt;
    }
}
