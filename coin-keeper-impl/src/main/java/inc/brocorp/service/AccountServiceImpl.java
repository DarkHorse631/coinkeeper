package inc.brocorp.service;


import inc.brocorp.exception.EntityException;
import inc.brocorp.model.Account;
import inc.brocorp.repo.AccountRepo;
import inc.brocorp.resource.dto.AccountDto;
import inc.brocorp.resource.dto.search.AccountSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepo accountRepo;
    private final MapperFacade mapperFacade;
    private static Logger log = Logger.getLogger(AccountServiceImpl.class.getName());

    @Value("${exception.doesNotExist}")
    private String exceptionDoesNotExist;

    public AccountServiceImpl(AccountRepo accountRepo, MapperFacade mapperFacade) {
        this.accountRepo = accountRepo;
        this.mapperFacade = mapperFacade;
    }

    @Transactional
    public AccountDto create(AccountDto accountDto) {
        accountDto.setId(UUID.randomUUID());
        Account account = mapperFacade.map(accountDto, Account.class);
        return mapperFacade.map(accountRepo.save(account), AccountDto.class);
    }

    @Transactional(readOnly = true)
    public AccountDto findById(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        if (!accountRepo.existsById(id)) throw new EntityException(exceptionDoesNotExist);
        Account account = accountRepo.findById(id).orElse(null);
        return mapperFacade.map(account, AccountDto.class);
    }

    @Transactional(readOnly = true)
    public List<AccountDto> findAllByUserId(UUID userId) {
        if (ObjectUtils.isEmpty(userId)) throw new IllegalArgumentException("Id is empty.");
        final List<AccountDto> result = new ArrayList<>();
        for (Account a : accountRepo.findAllByUserId(userId)) {
            result.add(mapperFacade.map(a, AccountDto.class));
        }
        return result;
    }

    @Transactional(readOnly = true)
    public PageDto<AccountDto> getAccounts(Search<AccountSearchDto> accountSearchDto) {
        Page<Account> page = accountRepo
                .findAll(getSpec(accountSearchDto.getData()), getOf(accountSearchDto));
        var accounts = page
                .map(account -> mapperFacade.map(account, AccountDto.class)).toList();
        final PageDto<AccountDto> pageDto = new PageDto<>();
        pageDto.setData(accounts);
        pageDto.setTotal(page.getTotalElements());
        return pageDto;
    }

    @Transactional
    public AccountDto update(AccountDto accountDto) {
        if (ObjectUtils.isEmpty(accountDto.getId())) throw new IllegalArgumentException("Id is empty.");
        if (!isExist(accountDto.getId())) throw new EntityException(exceptionDoesNotExist);
        Account account = mapperFacade.map(accountDto, Account.class);
        return mapperFacade.map(accountRepo.save(account), AccountDto.class);
    }

    @Transactional
    public void updateTotalSum(UUID id, BigDecimal amount, boolean isDecreasing) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        if (amount == null) throw new IllegalArgumentException("Amount is empty.");
        Account account = accountRepo.findById(id).orElse(null);
        if (account == null) throw new EntityException(exceptionDoesNotExist);
        BigDecimal newTotalSum = isDecreasing ? account.getTotalSum().subtract(amount) :
                account.getTotalSum().add(amount);
        accountRepo.updateTotalSum(id, newTotalSum, LocalDateTime.now());
    }

    @Transactional
    public void delete(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        if (!accountRepo.existsById(id)) throw new EntityException(exceptionDoesNotExist);
        accountRepo.deleteById(id);
    }

    @Transactional(readOnly = true)
    public boolean isExist(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        return accountRepo.existsById(id);
    }

    @Async
    void logInfo(String operationType, AccountDto accountDto) {
        log.info("Operation type: " + operationType + ", result: " + accountDto.toString());
    }

    private PageRequest getOf(Search<AccountSearchDto> accountSearchDto) {
        var page = accountSearchDto.getPage();
        return PageRequest.of(page.getPage(), page.getSize());
    }

    private Specification<Account> getSpec(AccountSearchDto accountSearchDto) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (!ObjectUtils.isEmpty(accountSearchDto.getUserId())) {
                predicates.add(builder.like(root.get("user_id"), accountSearchDto.getUserId().toString()));
            }
            if (!ObjectUtils.isEmpty(accountSearchDto.getTitle())) {
                predicates.add(builder.like(root.get("title"), accountSearchDto.getTitle()));
            }
            if (!ObjectUtils.isEmpty(accountSearchDto.getCreateAt())) {
                predicates.add(builder.equal(root.get("create_at"), accountSearchDto.getCreateAt()));
            }
            return builder.and(predicates.toArray(Predicate[]::new));
        };
    }
}
