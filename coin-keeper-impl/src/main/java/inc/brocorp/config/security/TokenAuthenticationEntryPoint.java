package inc.brocorp.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import inc.brocorp.resource.dto.ResponseError;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Component
public class TokenAuthenticationEntryPoint implements AuthenticationEntryPoint {

    private static final Logger log = LogManager.getLogger(TokenAuthenticationEntryPoint.class);

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        log.error(authException.getLocalizedMessage());
        ResponseError responseError = new ResponseError(UUID.randomUUID(), authException.getLocalizedMessage(), authException.getClass().getSimpleName());
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        response.getOutputStream().println(new ObjectMapper().writeValueAsString(responseError));
    }
}
