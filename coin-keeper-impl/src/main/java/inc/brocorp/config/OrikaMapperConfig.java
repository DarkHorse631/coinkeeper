package inc.brocorp.config;


import inc.brocorp.model.Account;
import inc.brocorp.model.MoneyTransfer;
import inc.brocorp.resource.dto.AccountDto;
import inc.brocorp.resource.dto.MoneyTransferDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import net.rakugakibox.spring.boot.orika.OrikaMapperFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

@Configuration
public class OrikaMapperConfig implements OrikaMapperFactoryConfigurer {

    @Bean
    DatatypeFactory datatypeFactory() throws DatatypeConfigurationException {
        return DatatypeFactory.newInstance();
    }

    @Bean
    MappingContext.Factory mappingFactory() {
        var factory = new MappingContext.Factory();
        new DefaultMapperFactory.Builder().mappingContextFactory(factory).build();
        return factory;
    }

    @Override
    public void configure(MapperFactory orikaMapperFactory) {
        orikaMapperFactory.classMap(Account.class, AccountDto.class)
                .byDefault()
                .register();

        orikaMapperFactory.classMap(AccountDto.class, Account.class)
                .byDefault()
                .register();


        orikaMapperFactory.classMap(MoneyTransfer.class, MoneyTransferDto.class)
                .byDefault()
                .register();
        orikaMapperFactory.classMap(MoneyTransferDto.class, MoneyTransfer.class)
                .byDefault()
                .register();
    }
}
