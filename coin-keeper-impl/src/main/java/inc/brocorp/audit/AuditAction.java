package inc.brocorp.audit;

public final class AuditAction {

    public static final String TRANSFER_CREATE = "TRANSFER_CREATE";
    public static final String TRANSFER_UPDATE = "TRANSFER_UPDATE";
    public static final String TRANSFER_DELETE = "TRANSFER_DELETE";
    public static final String ACCOUNT_CREATE = "ACCOUNT_CREATE";
    public static final String ACCOUNT_UPDATE = "ACCOUNT_UPDATE";
    public static final String ACCOUNT_DELETE = "ACCOUNT_DELETE";

    private AuditAction(){
    }
}
