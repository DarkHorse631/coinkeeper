package inc.brocorp.repo;

import inc.brocorp.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Класс содержащий API необходимый для работы с эдектронными счетами на уровне БД
 */
@Repository
public interface AccountRepo extends JpaRepository<Account, UUID>, JpaSpecificationExecutor<Account> {

    /**
     * Метод для поиска всех электронных счетов в БД приложения
     *
     * @return список всех электронных счетов в БД приложения
     */
    List<Account> findAllByUserId(UUID id);

    @Modifying
    @Query("update Account a set a.totalSum = :totalSum, updateAt = :updateAt where a.id = :id")
    void updateTotalSum(@Param("id") UUID id,@Param("totalSum") BigDecimal totalSum, @Param("updateAt") LocalDateTime updateAt);
}
