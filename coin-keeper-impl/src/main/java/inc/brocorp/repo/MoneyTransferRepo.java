package inc.brocorp.repo;

import inc.brocorp.model.MoneyTransfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Класс содержащий API необходимый для обработки денежных переводов на уровне БД
 */
@Repository
public interface MoneyTransferRepo extends JpaRepository<MoneyTransfer, UUID> , JpaSpecificationExecutor<MoneyTransfer> {

    /**
     * Метод для поиска всех денежных переводов определенного счета в БД приложения
     *
     * @param accountId - id счета для которого необходимо найте перводы
     * @return список всех денежных переводов на счете
     */
    List<MoneyTransfer> findAllByAccount_Id(UUID accountId);
}
