package inc.brocorp.service;

import inc.brocorp.Bootstrapper;
import inc.brocorp.exception.EntityException;
import inc.brocorp.model.Account;
import inc.brocorp.repo.AccountRepo;
import inc.brocorp.resource.dto.AccountDto;
import inc.brocorp.resource.dto.search.AccountSearchDto;
import inc.brocorp.resource.dto.search.Page;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@ContextConfiguration(classes = Bootstrapper.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AccountServiceTest {

    @Autowired
    AccountServiceImpl accountService;

    @Autowired
    MoneyTransferServiceImpl moneyTransferService;

    @Autowired
    MapperFacade mapperFacade;

    @Autowired
    AccountRepo accountRepo;

    @Test
    public void createTest(){
        when(mapperFacade.map(any(AccountDto.class), eq(Account.class))).thenReturn(new Account());
        accountService.create(new AccountDto());
        verify(accountRepo, times(1)).save(any(Account.class));
    }

    @Test
    public void findByNullId(){
        UUID id = null;
        assertThrows(IllegalArgumentException.class, () -> accountService.findById(id));
    }

    @Test
    public void findByIdButNotFound(){
        when(accountRepo.existsById(any(UUID.class))).thenReturn(false);
        assertThrows(EntityException.class, () -> accountService.findById(UUID.randomUUID()));
    }

    @Test
    public void findById(){
        UUID id = UUID.randomUUID();
        when(mapperFacade.map(any(), eq(AccountDto.class))).thenReturn(new AccountDto());
        when(accountRepo.existsById(any(UUID.class))).thenReturn(true);
        AccountDto accountDto = accountService.findById(id);
        assertNotNull(accountDto);
        verify(accountRepo, times(1)).findById(id);
    }

    @Test
    public void findAllByUserIdByNullId(){
        UUID id = null;
        assertThrows(IllegalArgumentException.class, () -> accountService.findAllByUserId(id));
    }

    @Test
    public void getAccountsTest(){
        Search<AccountSearchDto> accountSearchDto = getAccountSearchDto();
        var page = new PageImpl<>(List.of(new Account()));
        when(accountRepo.findAll(any(Specification.class), any(PageRequest.class))).thenReturn(page);
        when(mapperFacade.map(any(Account.class), eq(AccountDto.class))).thenReturn( new AccountDto());
        PageDto<AccountDto> result = accountService.getAccounts(accountSearchDto);
        assertEquals(1, result.getTotal());

    }

    @Test
    public void updateTotalSumWithNullId(){
        UUID id = null;
        BigDecimal sum = new BigDecimal("11.11");
        assertThrows(IllegalArgumentException.class, ()-> accountService.updateTotalSum(id, sum, false));
    }

    @Test
    public void updateTotalSumWithNullSum(){
        UUID id = UUID.randomUUID();
        assertThrows(IllegalArgumentException.class, ()-> accountService.updateTotalSum(id, null, false));
    }

    @Test
    public void updateTotalSum(){
        UUID id = UUID.randomUUID();
        Optional<Account> accountOptional = Optional.of(new Account());
        accountOptional.get().setTotalSum(BigDecimal.valueOf(110));
        when(accountRepo.findById(any())).thenReturn(accountOptional);
        accountService.updateTotalSum(id, BigDecimal.valueOf(1111.00), true);
        verify(accountRepo, times(1)).findById(id);
        verify(accountRepo, times(1)).updateTotalSum(eq(id), any(BigDecimal.class), any(LocalDateTime.class));
    }

    @Test
    public void deleteByIdWithNullId(){
        UUID id = null;
        BigDecimal sum = new BigDecimal("11.11");
        assertThrows(IllegalArgumentException.class, ()-> accountService.delete(id));
    }

    @Test
    public void deleteButNotFound(){
        when(accountRepo.existsById(any(UUID.class))).thenReturn(false);
        assertThrows(IllegalArgumentException.class, ()-> accountService.delete(UUID.randomUUID()));
    }

    @Test
    public void deleteTest(){
        UUID id = UUID.randomUUID();
        when(accountRepo.existsById(eq(id))).thenReturn(true);
        accountService.delete(id);
        verify(accountRepo, times(1)).existsById(id);
        verify(accountRepo, times(1)).deleteById(id);
    }

    public Search<AccountSearchDto> getAccountSearchDto(){
        AccountSearchDto asd = new AccountSearchDto();
        asd.setUserId(UUID.randomUUID());
        Search<AccountSearchDto> s = new Search<>();
        s.setData(asd);
        s.setPage(new Page(0,11));
        return s;
    }
}
