package inc.brocorp.service;

import inc.brocorp.Bootstrapper;
import inc.brocorp.exception.EntityException;
import inc.brocorp.model.Account;
import inc.brocorp.model.MoneyTransfer;
import inc.brocorp.repo.AccountRepo;
import inc.brocorp.repo.MoneyTransferRepo;
import inc.brocorp.resource.dto.MoneyTransferDto;
import inc.brocorp.resource.dto.search.MoneyTransferSearchDto;
import inc.brocorp.resource.dto.search.Page;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@ContextConfiguration(classes = Bootstrapper.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MoneyTransferServiceTest {

    @Autowired
    MoneyTransferServiceImpl subj;

    @Autowired
    AccountServiceImpl accountService;

    @Autowired
    AccountRepo accountRepo;

    @Autowired
    MoneyTransferRepo transferRepo;

    @Test
    public void create() {
        var dto = getMoneyTransferDto();
        AccountServiceImpl spy = Mockito.spy(accountService);
        when(accountService.isExist(dto.getAccountId())).thenReturn(true);
        Optional<Account> accountOptional = Optional.of(new Account());
        accountOptional.get().setTotalSum(BigDecimal.valueOf(110));
        when(accountRepo.findById(any())).thenReturn(accountOptional);
        when(transferRepo.save(any())).thenReturn(getMoneyTransfer());
        subj.create(dto);
        verify(transferRepo, times(1)).save(any(MoneyTransfer.class));
    }

    @Test
    public void createButNotFoundAccount() {
        assertThrows(EntityException.class, () -> subj.create(getMoneyTransferDto()));
    }

    @Test
    public void findByIdWithNullId() {
        assertThrows(IllegalArgumentException.class, () -> subj.findById(null));
    }

    @Test
    public void findByIdButNotFount() {
        assertThrows(EntityException.class, () -> subj.findById(UUID.randomUUID()));
    }

    @Test
    public void findAllByAccountIdTest() {
        UUID id = UUID.randomUUID();
        MoneyTransfer mt1 = getMoneyTransfer();
        mt1.getAccount().setId(id);
        MoneyTransfer mt2 = getMoneyTransfer();
        mt1.getAccount().setId(id);

        List<MoneyTransfer> result = List.of(mt1, mt2);
        when(transferRepo.findAllByAccount_Id(id)).thenReturn(result);

        List<MoneyTransferDto> transfers = subj.findAllByAccountId(id);

        assertEquals(2, transfers.size());
        verify(transferRepo, times(1)).findAllByAccount_Id(id);
    }

    @Test
    public void findAllByAccountIdWithNullId() {
        assertThrows(IllegalArgumentException.class, () -> subj.findAllByAccountId(null));
    }

    @Test
    public void updateWithNullId() {
        assertThrows(IllegalArgumentException.class, () -> subj.update(new MoneyTransferDto()));
    }

    @Test
    public void updateButNotFount() {
        var dto = getMoneyTransferDto();
        dto.setId(UUID.randomUUID());
        assertThrows(EntityException.class, () -> subj.update(dto));
    }

    @Test
    public void updateTest() {
        var dto = getMoneyTransferDto();
        dto.setId(UUID.randomUUID());
        when(transferRepo.existsById(dto.getId())).thenReturn(true);
        when(transferRepo.save(any())).thenReturn(getMoneyTransfer());
        var result = subj.update(dto);
        assertNotNull(result);
        verify(transferRepo, times(1)).save(any(MoneyTransfer.class));
    }

    @Test
    public void deleteWithNullId() {
        assertThrows(IllegalArgumentException.class, () -> subj.delete(null));
    }

    @Test
    public void deleteTest() {
        UUID id = UUID.randomUUID();

        var a = new Account();
        a.setId(UUID.randomUUID());

        Optional<MoneyTransfer> transferOptional = Optional.of(new MoneyTransfer());
        transferOptional.get().setAmount(BigDecimal.valueOf(110));
        transferOptional.get().setAccount(a);
        when(transferRepo.findById(any())).thenReturn(transferOptional);

        Optional<Account> accountOptional = Optional.of(new Account());
        accountOptional.get().setTotalSum(BigDecimal.valueOf(110));
        when(accountRepo.findById(any())).thenReturn(accountOptional);
        subj.delete(id);

        verify(transferRepo, times(1)).deleteById(id);
    }

    @Test
    public void getTransfersTest() {
        Search<MoneyTransferSearchDto> moneyTransferDto = getTransferSearchDto();
        var moneyTransfer = new MoneyTransfer();
        moneyTransfer.setAccount(new Account());
        moneyTransfer.setAmount(BigDecimal.valueOf(11.09));
        var page = new PageImpl<>(List.of(moneyTransfer));
        when(transferRepo.findAll(any(Specification.class), any(PageRequest.class))).thenReturn(page);
        PageDto<MoneyTransferDto> result = subj.getTransfers(moneyTransferDto);
        assertEquals(1, result.getTotal());

    }

    public MoneyTransferDto getMoneyTransferDto() {
        MoneyTransferDto dto = new MoneyTransferDto();
        dto.setAccountId(UUID.randomUUID());
        dto.setAmount("119.01");
        return dto;
    }

    public MoneyTransfer getMoneyTransfer() {
        MoneyTransfer mt = new MoneyTransfer();
        mt.setId(UUID.randomUUID());
        mt.setAccount(new Account());
        mt.setAmount(BigDecimal.valueOf(78.09));
        return mt;
    }

    public Search<MoneyTransferSearchDto> getTransferSearchDto() {
        MoneyTransferSearchDto mtd = new MoneyTransferSearchDto();
        mtd.setAccountId(UUID.randomUUID());
        Search<MoneyTransferSearchDto> s = new Search<>();
        s.setData(mtd);
        s.setPage(new Page(0, 11));
        return s;
    }
}
