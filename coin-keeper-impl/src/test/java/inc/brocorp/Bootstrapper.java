package inc.brocorp;

import inc.brocorp.repo.AccountRepo;
import inc.brocorp.repo.MoneyTransferRepo;
import inc.brocorp.service.EventService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@TestConfiguration
@Import(Bootstrapper.BeanInitializer.class)
public class Bootstrapper {

    @TestConfiguration
    @ComponentScan(basePackages = {"inc.brocorp.service"})
    public static class BeanInitializer {

        @MockBean
        AccountRepo accountRepo;

        @MockBean
        MapperFacade mapperFacade;

        @MockBean
        MoneyTransferRepo moneyTransferRepo;

        @MockBean
        EventService eventService;
    }
}
