package inc.brocorp.resource.controller;

import inc.brocorp.resource.dto.MoneyTransferDto;
import inc.brocorp.resource.dto.ResponseError;
import inc.brocorp.resource.dto.search.MoneyTransferSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

@Api(value = "API для работы с денежными переводами внутри счета пользователя")
@RequestMapping("/api/money_transfers")
public interface MoneyTransferController {

    @ApiOperation(value = "Создание денежного перевода")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Счет пользователя успешно создан",
                    response = MoneyTransferDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @PostMapping
    ResponseEntity<MoneyTransferDto> create(@RequestBody MoneyTransferDto moneyTransfer, UriComponentsBuilder componentsBuilder);

    @ApiOperation(value = "Поиск денежного перевода по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Денежный перевод найден",
                    response = MoneyTransferDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @ApiParam(name = "id", required = true)
    @GetMapping("/{id}")
    ResponseEntity<MoneyTransferDto> findById(@PathVariable UUID id);

    @ApiOperation(value = "Поиск денежных переводов с возможностью задать параметры")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Денежные переводы найдены",
                    response = MoneyTransferDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @GetMapping
    ResponseEntity<PageDto<MoneyTransferDto>> findAll(@RequestBody Search<MoneyTransferSearchDto> moneyTransferSearchDto);

    @ApiOperation(value = "Обновление данных денежного перевода")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Данные перевода обновлены",
                    response = MoneyTransferDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @ApiParam(name = "id", required = true)
    @PutMapping("/{id}")
    ResponseEntity<MoneyTransferDto> update(@PathVariable UUID id, @RequestBody MoneyTransferDto moneyTransfer);

    @ApiOperation(value = "Удаление денежного перевода по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Денежный перевод удален",
                    response = MoneyTransferSearchDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @ApiParam(name = "id", required = true)
    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable UUID id);
}
