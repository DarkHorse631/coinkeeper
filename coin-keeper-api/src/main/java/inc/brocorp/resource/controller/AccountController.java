package inc.brocorp.resource.controller;


import inc.brocorp.resource.dto.AccountDto;
import inc.brocorp.resource.dto.ResponseError;
import inc.brocorp.resource.dto.search.AccountSearchDto;
import inc.brocorp.resource.dto.search.PageDto;
import inc.brocorp.resource.dto.search.Search;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.UUID;

@RequestMapping("/api/accounts")
@Api(value = "API для работы со счетами пользователя")
public interface AccountController {

    @ApiOperation(value = "Создание счета пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Счет пользователя успешно создан",
                    response = AccountDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @PostMapping
    ResponseEntity<AccountDto> create(@RequestBody AccountDto account, UriComponentsBuilder componentsBuilder);


    @ApiOperation(value = "Поиск счета пользователя по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет пользователя найден",
                    response = AccountDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @ApiParam(name = "id", required = true)
    @GetMapping("/{id}")
    ResponseEntity<AccountDto> findById(@PathVariable UUID id);


    @ApiOperation(value = "Поиск счетов пользователя по критериям")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Счета пользователя успешно найдены",
                    response = PageDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @GetMapping
    ResponseEntity<PageDto<AccountDto>> findAll(@RequestBody Search<AccountSearchDto> accountSearchDto);


    @ApiOperation(value = "Обновление данных счета пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет пользователя успешно обновлен",
                    response = AccountDto.class),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @ApiParam(name = "id", required = true)
    @PutMapping("/{id}")
    ResponseEntity<AccountDto> update(@PathVariable UUID id, @RequestBody AccountDto account);


    @ApiOperation(value = "Удаление счета пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Счет пользователя успешно удален"),
            @ApiResponse(code = 400, message = "Непредвиденная ошибка", response = ResponseError.class),
            @ApiResponse(code = 401, message = "Не пройдена авторизация", response = ResponseError.class),
            @ApiResponse(code = 403, message = "Нет полномочий на выполнение запрашиваемой операции",
                    response = ResponseError.class)
    })
    @ApiParam(name = "id", required = true)
    @DeleteMapping("/{id}")
    ResponseEntity<Void> delete(@PathVariable UUID id);
}
