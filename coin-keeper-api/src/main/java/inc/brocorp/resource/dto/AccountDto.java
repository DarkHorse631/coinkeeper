package inc.brocorp.resource.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@ApiModel(value = "Базовая модель счета пользователя")
public class AccountDto implements Serializable {

    @ApiModelProperty(value = "Идентификатор счета", example = "6e328c1a-2e99-4635-9668-b1a437d1db18", allowEmptyValue = true)
    private UUID id;

    @ApiModelProperty(value = "Идентификатор пользователя", example = "6e328c1a-2e99-4635-9668-b1a437d1db18", required = true)

    private UUID userId;
    @ApiModelProperty(value = "Название счета", example = "Деньги на мечту", allowEmptyValue = true)
    private String title;

    @ApiModelProperty(value = "Сумма денег на счету", example = "215001.97", allowEmptyValue = true)
    private String totalSum;

    @ApiModelProperty(value = "Дата создания счета", example = "2021-02-02T02:26:47", allowEmptyValue = true)
    private LocalDateTime createAt;

    @ApiModelProperty(value = "Дата обновления счета", example = "2021-02-02T02:26:47", allowEmptyValue = true)
    private LocalDateTime updateAt;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(String totalSum) {
        this.totalSum = totalSum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public LocalDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(LocalDateTime updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return "AccountDto{" +
                "id=" + id +
                ", userId=" + userId +
                ", title='" + title + '\'' +
                ", totalSum='" + totalSum + '\'' +
                ", createAt=" + createAt +
                ", updateAt=" + updateAt +
                '}';
    }
}
