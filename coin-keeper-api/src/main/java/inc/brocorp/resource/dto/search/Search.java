package inc.brocorp.resource.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Базовая модель содержащая параметры поиска и пагинации")
public class Search<T> {

    @ApiModelProperty(value = "Объект содержащий параметры поиска")
    private T data;

    @ApiModelProperty(value = "Объект содержащий параметры пагинации")
    private Page page;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}
