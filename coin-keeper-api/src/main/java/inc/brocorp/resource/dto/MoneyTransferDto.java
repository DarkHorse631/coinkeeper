package inc.brocorp.resource.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@ApiModel(value = "Базовая модель денежного перевода")
public class MoneyTransferDto implements Serializable {

    @ApiModelProperty(value = "Идентификатор денежного перевода", example = "6e328c1a-2e99-4635-9668-b1a437d1db18", allowEmptyValue = true)
    private UUID id;

    @ApiModelProperty(value = "Идентификатор пользователя", example = "6e328c1a-2e99-4635-9668-b1a437d1db18", required = true)
    private UUID accountId;

    @ApiModelProperty(value = "Название денежного перевода", example = "Отложил с ЗП", allowEmptyValue = true)
    private String title;

    @ApiModelProperty(value = "Сумма денежного перевода", example = "22000.00")
    private String amount;

    @ApiModelProperty(value = "Дата создания денежного перевода", example = "2021-02-02T02:26:47", allowEmptyValue = true)
    private LocalDateTime createAt;

    @ApiModelProperty(value = "Дата обновления денежного перевода", example = "2021-02-02T02:26:47", allowEmptyValue = true)
    private LocalDateTime updateAt;

    public MoneyTransferDto() {

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public LocalDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(LocalDateTime updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return "MoneyTransferDto{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", title='" + title + '\'' +
                ", amount='" + amount + '\'' +
                ", createAt=" + createAt +
                ", updateAt=" + updateAt +
                '}';
    }
}
