package inc.brocorp.resource.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.UUID;

@ApiModel(value = "Базовая модель содержащая параметры поиска денежных переводов")
public class AccountSearchDto {

    @ApiModelProperty(value = "Идентификатор пользователя которому принадлежит счет", example = "6e328c1a-2e99-4635-9668-b1a437d1db18", allowEmptyValue = true)
    private UUID userId;

    @ApiModelProperty(value = "Название счета", example = "Отложил с ЗП", allowEmptyValue = true)
    private String title;

    @ApiModelProperty(value = "Дата создания", example = "2021-02-02T02:26:47", allowEmptyValue = true)
    private LocalDateTime createAt;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }
}
