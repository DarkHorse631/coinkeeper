package inc.brocorp.resource.dto.search;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;
import java.util.UUID;

@ApiModel(value = "Базовая модель содержащая параметры поиска денежных переводов")
public class MoneyTransferSearchDto {

    @ApiModelProperty(value = "Идентификатор счета к которому относится денежный перевод")
    private UUID accountId;

    @ApiModelProperty(value = "Название денежного перевода")
    private String title;

    @ApiModelProperty(value = "Дата создания")
    private LocalDateTime createAt;

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }
}
