package inc.brocorp.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import java.io.IOException;

/**
 * Утилита необходимая для конвертации java объектов в json  и обратно
 */
public final class JsonUtil<T> implements AttributeConverter<T, String> {

    private static final Logger log = LoggerFactory.getLogger(JsonUtil.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(T object) {
        String jsonString = null;
        try {
            jsonString = objectMapper.writeValueAsString(object);
        } catch (final JsonProcessingException e) {
            log.error("JSON writing error", e);
        }
        return jsonString;
    }

    @Override
    public T convertToEntityAttribute(String jsonString) {
        T object = null;
        try {
            object = objectMapper.readValue(jsonString, new TypeReference<>() {
            });
        } catch (final IOException e) {
            log.error("JSON reading error", e);
        }
        return object;
    }
}
