package inc.brocorp.enums;

/**
 * Тип события аудита используется в {@link inc.brocorp.model.Event}
 */
public enum EventType {
    STARTED("Событие стартовало"),
    SUCCESSFULLY("Событие успешно завершено"),
    FAILED("Событие провалено");

    private String description;

    EventType(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
