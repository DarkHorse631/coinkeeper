package inc.brocorp.service;

import inc.brocorp.annotation.Audit;
import inc.brocorp.enums.EventType;
import inc.brocorp.model.Event;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.lang.reflect.Method;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.UUID;

/**
 * Сервис для управления аспектами в приложении
 */
@Aspect
@Component
public class AspectService {

    private static final Logger log = LoggerFactory.getLogger(AspectService.class);

    private final EventService eventService;

    public AspectService(EventService eventService) {
        this.eventService = eventService;
    }

    @Pointcut("@annotation(inc.brocorp.annotation.Audit) && execution(public * *(..))")
    public void publicAspectMethod() {
    }

    @Around("publicAspectMethod()")
    public Object aspect(ProceedingJoinPoint joinPoint) throws Throwable {
        final UUID id = UUID.randomUUID();
        final Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        final String action = method.getAnnotation(Audit.class).action();
        final LocalDateTime startDate = createStartedEvent(id, action, joinPoint).getCreateAt();
        try {
            Object proceed = joinPoint.proceed();
            createSuccessfulEvent(id, action, getExecutionTime(startDate), proceed);
            return proceed;
        } catch (Throwable e) {
            createFailedEvent(id, action, getExecutionTime(startDate), e);
            throw e;
        }
    }

    private Event createStartedEvent(UUID id, String action, ProceedingJoinPoint joinPoint) {
        final Event event = new Event(id, action, EventType.STARTED, null);
        buildEvent(event);
        final Object[] args = joinPoint.getArgs();
        for (Object argument : args) {
            if(argument instanceof ServletUriComponentsBuilder){
                continue;
            }
            event.getParams().put(argument.getClass().getSimpleName(), argument);
        }
        eventService.create(event);
        return event;
    }

    private Event createSuccessfulEvent(UUID id, String action, Double executionTime, Object proceed) {
        final Event event = new Event(id, action, EventType.SUCCESSFULLY, executionTime);
        buildEvent(event);
        event.getReturnValue().put(proceed.getClass().getSimpleName(), proceed);
        eventService.create(event);
        return event;
    }

    private Event createFailedEvent(UUID id, String action, Double executionTime, Throwable e) {
        final Event event = new Event(id, action, EventType.FAILED, executionTime);
        buildEvent(event);
        event.getReturnValue().put("exception", e.getMessage());
        eventService.create(event);
        return event;
    }

    private void buildEvent(Event event) {
        String userName = SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof UserDetails ?
                ((UserDetails) (SecurityContextHolder.getContext().getAuthentication().getPrincipal())).getUsername() :
                "anonymous";
        event.setUsername(userName);
        event.setCreateAt(LocalDateTime.now());
        event.setParams(new HashMap<>());
        event.setReturnValue(new HashMap<>());
    }

    /**
     * Вычисляет время выполнения события
     *
     * @param startDate дата начала события
     * @return время выполнения в секундах с точностью до миллисекунд
     */
    private Double getExecutionTime(LocalDateTime startDate) {
        LocalDateTime endDate = LocalDateTime.now();
        return (double) Duration.between(startDate, endDate).toMillis() / 1000;
    }
}
