package inc.brocorp.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import inc.brocorp.model.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * Реализация интерфейса {@link EventService}.
 */
@Service
@ConditionalOnProperty(prefix = "audit.logging", name = {"db"}, havingValue = "false")
public class EventToLogbackService implements EventService {

    private static final Logger log = LoggerFactory.getLogger(EventToLogbackService.class);

    public void create(Event event) {
        try {
            final String message = new ObjectMapper().writeValueAsString(event);
            log.debug(message);
        } catch (JsonProcessingException e) {
            log.error(e.getLocalizedMessage());
        }
    }
}
