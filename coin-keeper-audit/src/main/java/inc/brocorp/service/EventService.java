package inc.brocorp.service;

import inc.brocorp.model.Event;

/**
 * Интерфейс для работы с "Событиями"
 */
public interface EventService {

    void create(Event event);
}
