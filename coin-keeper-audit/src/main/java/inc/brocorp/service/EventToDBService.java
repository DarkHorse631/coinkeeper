package inc.brocorp.service;

import inc.brocorp.model.Event;
import inc.brocorp.repo.EventRepo;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * Реализация интерфейса {@link EventService}. Работает с профилем development
 */
@Service
@ConditionalOnProperty(prefix = "audit.logging", name = {"db"}, havingValue = "true")
public class EventToDBService implements EventService {

    private final EventRepo eventRepo;

    public EventToDBService(EventRepo eventRepo) {
        this.eventRepo = eventRepo;
    }


    @Override
    public void create(Event event) {
        eventRepo.save(event);
    }
}
