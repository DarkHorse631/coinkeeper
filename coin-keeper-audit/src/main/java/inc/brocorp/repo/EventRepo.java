package inc.brocorp.repo;

import inc.brocorp.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Репозиторий, необходимый для сохранения событий аудита
 */
@Repository
public interface EventRepo extends JpaRepository<Event, UUID>{
}
