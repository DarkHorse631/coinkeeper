package inc.brocorp.model;

import inc.brocorp.enums.EventType;
import inc.brocorp.util.JsonUtil;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

/**
 * Класс описывающий событие Аудита
 */
@Entity
@Table(name = "events")
@IdClass(Event.EventCompositeId.class)
public class Event implements Serializable {

    /**
     * Идентификатор
     */
    @Id
    private UUID id;

    /**
     * Действие пользователя
     */
    @Column(name = "action")
    private String action;

    /**
     * Тип результата события аудита {@link EventType}
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "event_type")
    private EventType eventType;

    /**
     * Дата создания события
     */
    @Column(name = "create_at")
    private LocalDateTime createAt;

    /**
     * Имя пользователя запустившего событие
     */
    private String username;

    /**
     * Время выполнения метода
     */
    @Column(name = "execution_time")
    private Double executionTime;

    /**
     * Входные параметры метода
     */
    @Convert(converter = JsonUtil.class)
    @Column(name = "params", columnDefinition = "json")
    private Map<String, Object> params;

    /**
     * Результат метода
     */
    @Convert(converter = JsonUtil.class)
    @Column(name = "return_value", columnDefinition = "json")
    private Map<String, Object> returnValue;


    public Event(UUID id, String action, EventType eventType, LocalDateTime createAt, String username, Double executionTime) {
        this.id = id;
        this.action = action;
        this.eventType = eventType;
        this.createAt = createAt;
        this.username = username;
        this.executionTime = executionTime;
    }

    public Event(UUID id, String action, EventType eventType, Double executionTime) {
        this.id = id;
        this.action = action;
        this.eventType = eventType;
        this.executionTime = executionTime;
    }

    public Event() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAuditCode() {
        return action;
    }

    public void setAuditCode(String action) {
        this.action = action;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public Double getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Double executionTime) {
        this.executionTime = executionTime;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public Map<String, Object> getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(Map<String, Object> returnValue) {
        this.returnValue = returnValue;
    }

    /**
     * Вложенный статический класс использующийся в качестве композитного ключа для класса {@link Event}
     * Без композитного ключа события с одинаковыми UUID перезаписываются в БД
     */
    public static class EventCompositeId implements Serializable{

        private UUID id;
        private EventType eventType;

        public EventCompositeId() {
        }

        public EventCompositeId(UUID id, EventType eventType) {
            this.id = id;
            this.eventType = eventType;
        }

        public UUID getId() {
            return id;
        }

        public void setId(UUID id) {
            this.id = id;
        }

        public EventType getEventType() {
            return eventType;
        }

        public void setEventType(EventType eventType) {
            this.eventType = eventType;
        }

    }
}
