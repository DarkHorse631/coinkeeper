package inc.brocorp.service;

import inc.brocorp.resource.Currency;

import java.math.BigDecimal;
import java.util.List;

public interface ConverterService {

    /**
     * Перевод рублей в определенную валюту
     *
     * @param currency валютаБ в которую необходимо перевести рубли
     * @param sum      сумма которую необходимо конвертировать
     * @return результат перево рублей в валюту
     */
    BigDecimal convert(String currency, String sum);

    /**
     * Получения списка валют в которые можно конвертировать рубли
     *
     * @return список валют
     */
    List<Currency> getCurrencies();
}
