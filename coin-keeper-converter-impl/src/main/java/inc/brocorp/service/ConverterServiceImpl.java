package inc.brocorp.service;

import inc.brocorp.resource.Currency;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

@Service
public class ConverterServiceImpl implements ConverterService {

    @Override
    public BigDecimal convert(String currency, String sum) {
        Currency cur = Currency.valueOf(currency.toUpperCase());
        BigDecimal summa = new BigDecimal(sum);
        return summa.divide(cur.getValue(), 2, RoundingMode.HALF_UP);
    }

    @Override
    public List<Currency> getCurrencies() {
        return Arrays.asList(Currency.values());
    }
}
