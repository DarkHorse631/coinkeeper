package inc.brocorp.controller;

import inc.brocorp.resource.Currency;
import inc.brocorp.service.ConverterService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class ConverterControllerImpl implements Converter {

    private ConverterService converterService;

    public ConverterControllerImpl(ConverterService converterService) {
        this.converterService = converterService;
    }

    @Override
    public ResponseEntity<BigDecimal> convert(String currency, String sum) {
        try{
            return new ResponseEntity<>( converterService.convert(currency, sum),new HttpHeaders(), HttpStatus.OK);
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

    }

    @Override
    public ResponseEntity<List<Currency>> getCurrencies() {
        return new ResponseEntity<>( converterService.getCurrencies() ,new HttpHeaders(), HttpStatus.OK);
    }
}
