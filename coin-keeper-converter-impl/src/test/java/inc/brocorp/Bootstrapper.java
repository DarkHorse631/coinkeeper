package inc.brocorp;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;

@TestConfiguration
@ComponentScan(basePackages = {"inc.brocorp.service"})
public class Bootstrapper {
}
