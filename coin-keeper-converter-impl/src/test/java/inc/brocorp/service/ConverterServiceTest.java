package inc.brocorp.service;

import inc.brocorp.Bootstrapper;
import inc.brocorp.resource.Currency;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@ContextConfiguration(classes = Bootstrapper.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ConverterServiceTest {

    @Autowired
    ConverterServiceImpl subj;

    @Test
    public void convertWithWrongCurrency(){
        assertThrows(IllegalArgumentException.class, ()-> subj.convert("rub","11080980"));
    }

    @Test
    public void convertWithWrongSum(){
        assertThrows(IllegalArgumentException.class, ()-> subj.convert("rub","110809,80"));
    }

    @Test
    public void convertTest(){
        BigDecimal sum = BigDecimal.valueOf(15000.08);
        BigDecimal result = sum.divide(Currency.USD.getValue(), 2, RoundingMode.HALF_UP);
        BigDecimal resultOfExpression = subj.convert(Currency.USD.toString(), sum.toEngineeringString());
        assertEquals(result, resultOfExpression);
    }

    @Test
    public void getCurrenciesTest(){
        int count = Currency.values().length;
        assertEquals(count, subj.getCurrencies().size());
    }
}
