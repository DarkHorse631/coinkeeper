package inc.brocorp.config.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class UserPrincipal implements UserDetails {

    private final Set<GrantedAuthority> authorities;
    private final String password;
    private final String username;
    private final boolean isAccountNonExpired;
    private final boolean isAccountNonLocked;
    private final boolean isCredentialsNonExpired;
    private final boolean isEnabled;

    private UserPrincipal(Set<GrantedAuthority> authorities, String password, String username,
                          boolean isAccountNonExpired, boolean isAccountNonLocked, boolean isCredentialsNonExpired,
                          boolean isEnabled) {
        this.authorities = authorities;
        this.password = password;
        this.username = username;
        this.isAccountNonExpired = isAccountNonExpired;
        this.isAccountNonLocked = isAccountNonLocked;
        this.isCredentialsNonExpired = isCredentialsNonExpired;
        this.isEnabled = isEnabled;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public static UserPrincipalBuilder builder() {
        return new UserPrincipalBuilder();
    }

    /**
     * Класс-builder для класса {@link UserPrincipal}
     */
    public static class UserPrincipalBuilder {

        private Set<GrantedAuthority> authorities;
        private String password;
        private String username;
        private boolean accountNonExpired;
        private boolean accountNonLocked;
        private boolean credentialsNonExpired;
        private boolean enabled;

        public UserPrincipalBuilder roles(String... roles) {
            if (!Objects.isNull(roles)) {
                Set<GrantedAuthority> authoritiesSet = new HashSet<>(roles.length);
                for (String role : roles) {
                    Assert.isTrue(!role.startsWith("ROLE_"),
                            String.format("%s cannot starts with 'ROLE_', it is added automatically", role));
                    authoritiesSet.add(new SimpleGrantedAuthority("ROLE_" + role));
                }
                this.authorities = authoritiesSet;
            }
            return this;
        }

        public UserPrincipalBuilder password(String password) {
            Assert.isTrue(!ObjectUtils.isEmpty(password), "Empty password field");
            this.password = password;
            return this;
        }

        public UserPrincipalBuilder username(String username) {
            Assert.isTrue(!ObjectUtils.isEmpty(username), "Empty password field");
            this.username = username;
            return this;
        }

        public UserPrincipalBuilder accountNonExpired(boolean accountNonExpired) {
            this.accountNonExpired = accountNonExpired;
            return this;
        }

        public UserPrincipalBuilder accountNonLocked(boolean accountNonLocked) {
            this.accountNonLocked = accountNonLocked;
            return this;
        }

        public UserPrincipalBuilder credentialsNonExpired(boolean credentialsNonExpired) {
            this.credentialsNonExpired = credentialsNonExpired;
            return this;
        }

        public UserPrincipalBuilder enabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public UserPrincipal build() {
            return new UserPrincipal(this.authorities, this.password, this.username, this.accountNonExpired,
                    this.accountNonLocked, this.credentialsNonExpired, this.enabled);
        }
    }
}
