package inc.brocorp.config.security;

import inc.brocorp.model.JwtClaims;
import inc.brocorp.service.JwtValidationService;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Optional;

@Service
public class TokenAuthenticationManager implements AuthenticationManager {

    private String applicationName;

    private final JwtValidationService validationService;
    private final ApplicationContext applicationContext;

    public TokenAuthenticationManager(JwtValidationService validationService, ApplicationContext applicationContext) {
        this.validationService = validationService;
        this.applicationContext = applicationContext;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        String token = Optional.ofNullable(String.valueOf(authentication.getCredentials())).orElse(null);
        if (ObjectUtils.isEmpty(token)) {
            authentication.setAuthenticated(false);
            return authentication;
        }
        JwtClaims claims = validationService.getClaimsFromJwt(token);
        validationService.validateJwtClaims(claims, applicationContext.getId());
        UserPrincipal userPrincipal = UserPrincipal
                .builder()
                .roles(claims.getRole())
                .username(claims.getSubject())
                .password("none")
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .enabled(true)
                .build();
        TokenAuthentication tokenAuthentication = new TokenAuthentication(userPrincipal, token);
        tokenAuthentication.setAuthenticated(true);
        return tokenAuthentication;
    }
}
