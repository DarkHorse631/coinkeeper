package inc.brocorp.config.security;

import inc.brocorp.service.JwtValidationService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.sql.DataSource;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private final ApplicationContext applicationContext;
    private final JwtValidationService jwtValidationService;
    private final TokenAuthenticationEntryPoint tokenAuthenticationEntryPoint;

    public SpringSecurityConfig(JwtValidationService jwtValidationService,
                                TokenAuthenticationEntryPoint tokenAuthenticationEntryPoint,
                                ApplicationContext applicationContext) {
        this.jwtValidationService = jwtValidationService;
        this.tokenAuthenticationEntryPoint = tokenAuthenticationEntryPoint;
        this.applicationContext = applicationContext;
    }


    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
       http
               .csrf().disable()
               .formLogin().disable()
               .authorizeRequests()
               .antMatchers("/api/auth",
                       "/api/auth/**",
                       "/actuator/**",
                       "/v2/api-docs",
                       "/swagger-resources/**",
                       "/swagger-ui.html",
                       "/swagger-ui/**"
               ).permitAll()
               .anyRequest().authenticated()
               .and().addFilterAt(authenticationFilter(), UsernamePasswordAuthenticationFilter.class)
               .authorizeRequests()
               .and().exceptionHandling()
               .authenticationEntryPoint(tokenAuthenticationEntryPoint);
    }

    public TokenAuthenticationFilter authenticationFilter() {
        TokenAuthenticationFilter filter = new TokenAuthenticationFilter("/api/v1/**");
        filter.setAuthenticationManager(new TokenAuthenticationManager(jwtValidationService, applicationContext));
        return filter;
    }
}
