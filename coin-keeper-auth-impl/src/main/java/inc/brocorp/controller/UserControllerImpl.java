package inc.brocorp.controller;


import inc.brocorp.annotation.Audit;
import inc.brocorp.audit.AuditAction;
import inc.brocorp.config.security.TokenAuthentication;
import inc.brocorp.config.security.UserPrincipal;
import inc.brocorp.model.UserDto;
import inc.brocorp.model.UserIdentificationDto;
import inc.brocorp.model.UserPrincipalDto;
import inc.brocorp.model.search.PageDto;
import inc.brocorp.model.search.Search;
import inc.brocorp.model.search.UserSearchDto;
import inc.brocorp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

@RestController
public class UserControllerImpl implements UserController {

    private final UserService userService;
    private final static Logger log = LoggerFactory.getLogger(UserControllerImpl.class);

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    @Audit(action = AuditAction.USER_CREATE)
    public ResponseEntity<UserDto> create(@RequestBody UserIdentificationDto user, UriComponentsBuilder componentsBuilder) {
        log.debug("Method 'create' start with {}", user);
        UserDto userDto = userService.create(user);
        URI uri = componentsBuilder.path("/api/users/" + userDto.getId()).buildAndExpand(userDto).toUri();
        log.debug("Method 'create' end with {}", userDto);
        return ResponseEntity.created(uri).body(userDto);
    }

    @Override
    public ResponseEntity<UserDto> findById(@PathVariable UUID id) {
        log.debug("Method 'findById' start with id {}", id);
        UserDto userDto = userService.findById(id);
        log.debug("Method 'findById' end with {}", userDto);
        return new ResponseEntity<>(userDto, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PageDto<UserDto>> findAll(@RequestBody Search<UserSearchDto> userSearch) {
        log.debug("Method 'findAll' start with {}", userSearch);
        PageDto<UserDto> users = userService.getUsers(userSearch);
        log.debug("Method 'findAll' find {} records", users.getTotal());
        return new ResponseEntity<>(users, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    @Audit(action = AuditAction.USER_UPDATE)
    public ResponseEntity<UserDto> update(@PathVariable UUID id, @RequestBody UserDto user) {
        log.debug("Method 'update' start with id {} and {}", id, user);
        if (!Objects.equals(id, user.getId())){
            throw new IllegalArgumentException("Id = " + user.getId() + " Expected same as : " + id);
        }
        UserDto userDto = userService.update(user);
        log.debug("Method 'update' end with {}", userDto);
        return new ResponseEntity<>(userDto, new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    @Audit(action = AuditAction.USER_DELETE)
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        log.debug("Method 'delete' start with id {}", id);
        userService.delete(id);
        log.debug("Method 'delete' with id {} ended", id);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }

    @Override
    @Audit(action = AuditAction.INFO)
    public ResponseEntity<UserPrincipalDto> getInfo() {
        log.debug("Method 'INFO' start");
        TokenAuthentication tokenAuthentication = (TokenAuthentication) (SecurityContextHolder.getContext().getAuthentication());
        UserPrincipal principal = (UserPrincipal) tokenAuthentication.getPrincipal();
        UserPrincipalDto userInfoDTO = UserPrincipalDto.builder()
                .setUsername(principal.getUsername())
                .setAccountNonExpired(principal.isAccountNonExpired())
                .setAccountNonLocked(principal.isAccountNonLocked())
                .setCredentialsNonExpired(principal.isCredentialsNonExpired())
                .setEnabled(principal.isEnabled())
                .setToken(tokenAuthentication.getToken())
                .setAuthenticated(tokenAuthentication.isAuthenticated())
                .setAuthorities(Arrays.asList(principal.getAuthorities().toArray())).build();
        log.debug("Method 'INFO' end");
        return ResponseEntity.ok(userInfoDTO);
    }
}
