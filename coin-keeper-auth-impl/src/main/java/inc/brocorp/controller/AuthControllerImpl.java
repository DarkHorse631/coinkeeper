package inc.brocorp.controller;

import inc.brocorp.annotation.Audit;
import inc.brocorp.audit.AuditAction;
import inc.brocorp.model.TokenDto;
import inc.brocorp.model.UserAuthDto;
import inc.brocorp.service.AuthService;
import org.apache.tomcat.websocket.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class AuthControllerImpl implements AuthController {

    private final AuthService authService;
    private final static Logger log = LoggerFactory.getLogger(AuthControllerImpl.class);

    public AuthControllerImpl(AuthService authService) {
        this.authService = authService;
    }

    @Override
    @Audit(action = AuditAction.AUTHENTICATE)
    public ResponseEntity<TokenDto> authenticate(UserAuthDto userAuthDto) {
        try {
            log.debug("Method 'authenticate' start for {}", userAuthDto.getUsername());
            TokenDto tokenDto = authService.authenticate(userAuthDto);
            log.debug("Method 'authenticate' end for {}", userAuthDto.getUsername());
            return new ResponseEntity<>(tokenDto,new HttpHeaders(), HttpStatus.OK);
        } catch (AuthenticationException e) {
            log.error("Method 'authenticate' end with error for {}", userAuthDto.getUsername());
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage(), e);
        }
    }
}
