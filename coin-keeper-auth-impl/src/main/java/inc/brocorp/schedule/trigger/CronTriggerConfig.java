package inc.brocorp.schedule.trigger;

import inc.brocorp.schedule.job.CronJob;
import inc.brocorp.schedule.job.TimeJob;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(prefix = "scheduling", name = {"enabled"})
public class CronTriggerConfig extends AbstractTrigger{

    @Bean
    public JobDetailFactoryBean cronJobDetail() {
        return createJobDetail(CronJob.class, CronJob.class.getSimpleName(),
                "Удаление пользователей, которые не были активированы в течении месяца после регистрации.");
    }

    @Bean
    public JobDetailFactoryBean timeJobDetail() {
        return createJobDetail(TimeJob.class, TimeJob.class.getSimpleName(),
                "Удаление успешно доставленных технических сообщений");
    }

    @Bean(name = "cronTrigger")
    public CronTriggerFactoryBean cronTrigger(@Qualifier("cronJobDetail") JobDetail jobDetail,
                                              @Value("${scheduling.cron.deleting.period}") String cron) {
        return createCronTrigger(jobDetail, cron, "cronTrigger",
                "Триггер для запуска удаления неактивированных пользователей");
    }

    @Bean(name = "timeTrigger")
    public SimpleTriggerFactoryBean timeTrigger(@Qualifier("timeJobDetail") JobDetail jobDetail,
                                                @Value("${scheduling.time.period}") Long time) {
        return createTimeTrigger(jobDetail, time, "cronTrigger",
                "Триггер для запуска удаления успешно доставленных сообщений");
    }
}
