package inc.brocorp.schedule.trigger;

import inc.brocorp.controller.AuthControllerImpl;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

/**
 * AbstractTrigger.
 * Служит для описания базового функционала для создания тригера на {@link Job} в кластерной системе.
 * Позволяет создать {@link JobDetailFactoryBean}. Также тригеры по крону {@link CronTriggerFactoryBean}
 * и таймоуту {@link SimpleTriggerFactoryBean}. Для этих целей необходимо унаследовать данный класс,
 * в котором настроить 2 основных бина:  {@link JobDetailFactoryBean} саму задачу и правило для ее обработки.
 * Всегда стоит посмотреть уже реализованные примеры и сделать по аналогии.
 * Создание каких-либо шедулеров при горизонтальном маштабировании запрещено.
 * Только на основании кластера с использованием данного триггера.
 * Создавать можно только через данный способ. Иначе при кластеризации(масштабирование) сервиса будут
 * получены дубликаты срабатывания джобов в системе, что может привести
 * к порче данных(потери, дубликатов, блокирование процессов друг другом и т.д.)
 *
 */
public class AbstractTrigger {

    private final static Logger log = LoggerFactory.getLogger(AbstractTrigger.class);

    /**
     * Создаёт{@link JobDetailFactoryBean} для запуска в Spring Quartz
     *
     * @param jobClass    класс являющийся реализацией {@link Job}, на который будет создана джоба.
     * @param name        имя джобы
     * @param description описание джобы
     * @return {@link JobDetailFactoryBean}
     */
    public JobDetailFactoryBean createJobDetail(Class<? extends Job> jobClass,
                                                String name, String description) {
        log.debug("create Job Detail for {} with class {}", name, jobClass);
        var detailFactoryBean = new JobDetailFactoryBean();
        detailFactoryBean.setJobClass(jobClass);
        detailFactoryBean.setName(name);
        detailFactoryBean.setDescription(description);
        detailFactoryBean.setDurability(true);
        return detailFactoryBean;
    }

    /**
     * Создаёт крон правило {@link CronTriggerFactoryBean} для запуска в Spring Quartz
     *
     * @param jobDetail      {@link JobDetail}, на который будет создан триггер.
     * @param cronExpression крон выражения для задания правила запуска триггера.
     * @param name           имя триггера.
     * @param description    описание триггера.
     * @return {@link CronTriggerFactoryBean}
     */
    public CronTriggerFactoryBean createCronTrigger(JobDetail jobDetail, String cronExpression,
                                                    String name, String description) {
        log.debug("create Trigger for {} with cron {}", name, cronExpression);
        var factoryBean = new CronTriggerFactoryBean();
        factoryBean.setJobDetail(jobDetail);
        factoryBean.setCronExpression(cronExpression);
        factoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);
        factoryBean.setDescription(description);
        factoryBean.setName(name);
        return factoryBean;
    }

    /**
     * Создаёт правило {@link SimpleTriggerFactoryBean} для запуска в Spring Quartz
     *
     * @param jobDetail   {@link JobDetail}, на который будет создан триггер.
     * @param interval    интервал, с которым выполняется джоба
     * @param name        имя триггера
     * @param description описание триггера
     * @return {@link SimpleTriggerFactoryBean}
     */
    public SimpleTriggerFactoryBean createTimeTrigger(JobDetail jobDetail, Long interval,
                                                      String name, String description) {
        log.debug("create  Trigger for {} name with interval {} interval", name, interval);
        var triggerFactoryBean = new SimpleTriggerFactoryBean();
        triggerFactoryBean.setJobDetail(jobDetail);
        triggerFactoryBean.setStartDelay(interval);
        triggerFactoryBean.setRepeatInterval(interval);
        triggerFactoryBean.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
        triggerFactoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
        triggerFactoryBean.setName(name);
        triggerFactoryBean.setDescription(description);
        return triggerFactoryBean;
    }
}

