package inc.brocorp.schedule.job;

import inc.brocorp.enums.MessageStatus;
import inc.brocorp.model.UserInfo;
import inc.brocorp.repo.MessageRepo;
import inc.brocorp.service.UserServiceImpl;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Класс описывающий задачу по удалению успешно отправленных сообщений
 */
public class TimeJob implements Job {

    private final MessageRepo messageRepo;
    private final static Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    public TimeJob(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }

    @Override
    public void execute(JobExecutionContext context) {
        List<UserInfo> successfulMessages = messageRepo.findAllByMessageStatus(MessageStatus.SUCCESS);
        messageRepo.deleteAll(successfulMessages);
        log.info("Deletion of delivered messages has started. Removed: {}", successfulMessages.size());
    }
}
