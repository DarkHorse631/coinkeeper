package inc.brocorp.schedule.job;

import inc.brocorp.service.UserServiceImpl;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.time.LocalDateTime;

/**
 * Класс описывающий задачу по удалению пользователей, которые не были активированы
 * в течении месяца после регистрации
 */
public class CronJob implements Job {

    private final UserServiceImpl userService;

    public CronJob(UserServiceImpl userService) {
        this.userService = userService;
    }

    @Override
    public void execute(JobExecutionContext context){
        LocalDateTime date = LocalDateTime.now().minusDays(30L);
        userService.findAllDraftUsers().forEach(user -> {
            if (user.getCreateAt().isBefore(date)) {
                userService.delete(user.getId());
            }
        });
    }
}
