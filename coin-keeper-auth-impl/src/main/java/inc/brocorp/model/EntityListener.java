package inc.brocorp.model;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

/**
 * Класс выполняющий простановку дат создания и обновления пользователя
 */
public class EntityListener {

    @PrePersist
    public void beforeCreate(User user){
        user.setCreateAt(LocalDateTime.now());
        user.setUpdateAt(LocalDateTime.now());
    }

    @PreUpdate
    public void beforeUpdate(User user){
        user.setUpdateAt(LocalDateTime.now());
    }
}
