package inc.brocorp.model;


import inc.brocorp.enums.Role;
import inc.brocorp.enums.UserStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Класс аккаунта пользователя
 */
@Entity
@Table(name = "users", schema = "coin_keeper_auth")
@EntityListeners(EntityListener.class)
public class User {

    /**
     * Идентификатор пользователя
     */
    @Id
    private UUID id;

    /**
     * Логин пользователя
     */
    @Column(unique = true, nullable = false)
    private String username;

    /**
     * Пароль пользователя
     */
    @Column(nullable = false)
    private String password;

    /**
     * Почта пользователя
     */
    @Column(unique = true, nullable = false)
    private String email;

    /**
     * Дата создания пользователя
     */
    @Column(name = "create_at")
    private LocalDateTime createAt;

    /**
     * Дата обновления пользователя
     */
    @Column(name = "update_at")
    private LocalDateTime updateAt;

    /**
     * Роль пользователя
     */
    @Enumerated(EnumType.STRING)
    private Role role;

    /**
     * Статус пользователя
     */
    @Enumerated(EnumType.STRING)
    private UserStatus status;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public LocalDateTime getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(LocalDateTime updateAt) {
        this.updateAt = updateAt;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
