package inc.brocorp.model;

import inc.brocorp.dto.UserInfoDto;
import inc.brocorp.enums.MessageStatus;
import inc.brocorp.enums.UserAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "users_info", schema = "coin_keeper_auth")
public class UserInfo {

    @Id
    private UUID id;

    @Column(name = "message_status")
    @Enumerated(EnumType.STRING)
    private MessageStatus messageStatus;

    @Column(name = "user_id")
    private UUID userId;

    @Enumerated(EnumType.STRING)
    private UserAction action;

    @Column(name = "create_at")
    private LocalDateTime createAt;

    public UUID getId() {
        return id;
    }

    public UserInfo(UUID id, MessageStatus messageStatus, UUID userId, LocalDateTime createAt) {
        this.id = id;
        this.messageStatus = messageStatus;
        this.userId = userId;
        this.createAt = createAt;
    }

    public UserInfo(UserInfoDto userInfoDto){
        this.id = userInfoDto.getId();
        this.messageStatus = userInfoDto.getMessageStatus();
        this.userId = userInfoDto.getUserId();
        this.action = userInfoDto.getAction();
        this.createAt = userInfoDto.getCreateAt();
    }

    public UserInfo() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public MessageStatus getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(MessageStatus messageStatus) {
        this.messageStatus = messageStatus;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    public UserAction getAction() {
        return action;
    }

    public void setAction(UserAction action) {
        this.action = action;
    }
}
