package inc.brocorp.jms;


import inc.brocorp.dto.UserInfoDto;
import inc.brocorp.model.UserInfo;
import inc.brocorp.repo.MessageRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageListener {

    private final static Logger log = LoggerFactory.getLogger(MessageListener.class);
    private final MessageRepo messageRepo;

    public MessageListener(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }

    @JmsListener(destination = "${inc.brocorp.queue.user-id-response}")
    public void receiveMessage(final UserInfoDto message) {
        log.info("Received message from Supreme service {}", message.toString());
        UserInfo oldUserInfo = messageRepo.findById(message.getId()).orElse(null);
        if(oldUserInfo == null){
            throw new IllegalArgumentException(String.format("Message with id = %s not found", message.getId().toString()));
        }
        oldUserInfo.setMessageStatus(message.getMessageStatus());
        messageRepo.save(oldUserInfo);
    }

}
