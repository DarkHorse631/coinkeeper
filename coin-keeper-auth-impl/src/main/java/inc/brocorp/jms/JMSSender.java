package inc.brocorp.jms;

import inc.brocorp.dto.UserInfoDto;

import inc.brocorp.enums.MessageStatus;
import inc.brocorp.model.UserInfo;
import inc.brocorp.repo.MessageRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class JMSSender {

    private final static Logger log = LoggerFactory.getLogger(JMSSender.class);
    private final JmsTemplate jmsTemplate;
    private final MessageRepo messageRepo;


    public JMSSender(JmsTemplate jmsTemplate, MessageRepo messageRepo) {
        this.jmsTemplate = jmsTemplate;
        this.messageRepo = messageRepo;
    }

    @Transactional
    public void sendMessage(final String queueName, final UserInfoDto userInfoDto) {
        userInfoDto.setId(UUID.randomUUID());
        userInfoDto.setCreateAt(LocalDateTime.now());
        userInfoDto.setMessageStatus(MessageStatus.SENT);
        messageRepo.save(new UserInfo(userInfoDto));
        jmsTemplate.setTimeToLive(30000);
        jmsTemplate.convertAndSend(queueName, userInfoDto);
        log.info("Sending user info {} to queue - {}", userInfoDto, queueName);
    }


}
