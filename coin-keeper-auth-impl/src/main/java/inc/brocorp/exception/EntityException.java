package inc.brocorp.exception;

public class EntityException extends IllegalArgumentException {
    public EntityException(String message) {
        super(message);
    }
}
