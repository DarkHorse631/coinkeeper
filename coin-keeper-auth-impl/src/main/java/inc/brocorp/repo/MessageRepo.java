package inc.brocorp.repo;

import inc.brocorp.enums.MessageStatus;
import inc.brocorp.model.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MessageRepo extends JpaRepository<UserInfo, UUID> {

    List<UserInfo> findAllByMessageStatus(MessageStatus messageStatus);
}
