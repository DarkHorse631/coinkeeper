package inc.brocorp.repo;

import inc.brocorp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Класс содержащий API необходимый для обработки аккаунтов пользователей на уровне БД
 */
@Repository
public interface UserRepo extends JpaRepository<User, UUID>, JpaSpecificationExecutor<User> {

    User findByUsername(String username);
}
