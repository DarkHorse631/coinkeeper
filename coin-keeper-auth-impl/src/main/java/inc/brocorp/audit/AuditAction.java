package inc.brocorp.audit;

public final class AuditAction {

    public static final String USER_CREATE = "USER_CREATE";
    public static final String USER_UPDATE = "USER_UPDATE";
    public static final String USER_DELETE = "USER_DELETE";
    public static final String AUTHENTICATE = "AUTHENTICATE";
    public static final String INFO = "INFO";

    private AuditAction(){
    }
}
