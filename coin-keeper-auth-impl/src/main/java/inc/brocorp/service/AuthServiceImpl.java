package inc.brocorp.service;

import inc.brocorp.exception.AuthenticationException;
import inc.brocorp.model.TokenDto;
import inc.brocorp.model.User;
import inc.brocorp.model.UserAuthDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Value("${security.secret-key}")
    private String secretKey;

    @Value(value = "${security.token.lifetime:60000}" )
    private long tokenLifetime;

    @Value("${security.token.application-name}")
    private String applicationName;

    public AuthServiceImpl(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public TokenDto authenticate(UserAuthDto userAuthDto) throws AuthenticationException {
        if (ObjectUtils.isEmpty(userAuthDto.getUsername()))
            throw new IllegalArgumentException("Username can't be empty");
        if (ObjectUtils.isEmpty(userAuthDto.getPassword()))
            throw new IllegalArgumentException("Password can't be empty");
        final User user = userService.findByUsername(userAuthDto.getUsername());
        if(user == null) throw new AuthenticationException("Wrong username or password!");
        if (!passwordEncoder.matches(userAuthDto.getPassword(), user.getPassword())) {
            throw new AuthenticationException("Wrong username or password!");
        }
        return new TokenDto(generateToken(userAuthDto, user));
    }

    private String generateToken(UserAuthDto userAuthDto, User user){
        Map<String, Object> payload = new HashMap<>();
        payload.put("role", user.getRole().name());
        payload.put("email", user.getEmail());
        Key key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKey));
        return Jwts.builder()
                .setClaims(payload)
                .setSubject(user.getUsername())                                 //Для кого выдан токен
                .setAudience(userAuthDto.getAudience())                         //Для какой системы выдан
                .setIssuedAt(new Date(System.currentTimeMillis()))              //Когда выдан
                .setExpiration(new Date(System.currentTimeMillis() + tokenLifetime))    //До скольки действителен токен
                .setId(UUID.randomUUID().toString())
                .setIssuer(applicationName)                                     //Система, выдавшая токен
                .signWith(key, SignatureAlgorithm.HS512).compact();

    }
}
