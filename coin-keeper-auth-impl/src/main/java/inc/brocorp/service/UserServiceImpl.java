package inc.brocorp.service;

import inc.brocorp.dto.UserInfoDto;
import inc.brocorp.enums.Role;
import inc.brocorp.enums.UserAction;
import inc.brocorp.enums.UserStatus;
import inc.brocorp.exception.EntityException;

import inc.brocorp.model.User;
import inc.brocorp.model.UserDto;
import inc.brocorp.model.UserIdentificationDto;
import inc.brocorp.model.search.PageDto;
import inc.brocorp.model.search.Search;
import inc.brocorp.model.search.UserSearchDto;
import inc.brocorp.repo.UserRepo;
import inc.brocorp.jms.JMSSender;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final MapperFacade mapperFacade;
    private final static Logger log = Logger.getLogger(UserServiceImpl.class.getName());
    private final PasswordEncoder passwordEncoder;
    private final JMSSender jmsSender;

//    @Value("${exception.isExist}")
    private String exceptionIsExist;

//    @Value("${exception.doesNotExist}")
    private String exceptionDoesNotExist;

    @Value("${inc.brocorp.queue.user-id-send}")
    private String queueName;

    public UserServiceImpl(UserRepo userRepo, MapperFacade mapperFacade, PasswordEncoder passwordEncoder, JMSSender jmsSender) {
        this.userRepo = userRepo;
        this.mapperFacade = mapperFacade;
        this.passwordEncoder = passwordEncoder;
        this.jmsSender = jmsSender;
    }

    @Transactional
    public UserDto create(UserIdentificationDto userDto) {
        userDto.setId(UUID.randomUUID());
        userDto.setStatus(UserStatus.DRAFT);
        userDto.setRole(Role.USER);
        if (userRepo.existsById(userDto.getId())) throw new EntityException(exceptionIsExist);
        if (ObjectUtils.isEmpty(userDto.getUsername())) throw new IllegalArgumentException("Username cannot be empty.");
        if (ObjectUtils.isEmpty(userDto.getEmail())) throw new IllegalArgumentException("Email cannot be empty.");
        if (!userDto.getPassword().equals(userDto.getRepeatPassword()))
            throw new RuntimeException("Passwords does not match.");
        User user = mapperFacade.map(userDto, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user = userRepo.save(user);
        final UserDto createdUser = mapperFacade.map(user, UserDto.class);
        logInfo("CREATE", createdUser);
        jmsSender.sendMessage(queueName, new UserInfoDto(user.getId(), UserAction.CREATE));
        return createdUser;
    }

    @Transactional(readOnly = true)
    public UserDto findById(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        if (!userRepo.existsById(id)) throw new EntityException(exceptionDoesNotExist);
        final User user = userRepo.findById(id).get();
        return mapperFacade.map(user, UserDto.class);
    }

    @Transactional(readOnly = true)
    public User findByUsername(String username){
        if(ObjectUtils.isEmpty(username)) throw new IllegalArgumentException("Username is empty.");
        return userRepo.findByUsername(username);
    }

    @Transactional(readOnly = true)
    public List<UserDto> findAll() {
        final List<UserDto> result = new ArrayList<>();
        for (User u : userRepo.findAll()) {
            result.add(mapperFacade.map(u, UserDto.class));
        }
        return result;
    }

    @Transactional
    public UserDto update(UserDto userDto) {
        if (ObjectUtils.isEmpty(userDto.getId())) throw new IllegalArgumentException("Id is empty.");
        if (!userRepo.existsById(userDto.getId())) throw new EntityException(exceptionDoesNotExist);
        final User user = mapperFacade.map(userDto, User.class);
        logInfo("UPDATE", userDto);
        return mapperFacade.map(userRepo.save(user), UserDto.class);
    }

    @Transactional
    public void delete(UUID id) {
        if (!userRepo.existsById(id)) throw new EntityException(exceptionDoesNotExist);
        final User user = userRepo.findById(id).get();
        user.setStatus(UserStatus.DISABLED);
        userRepo.save(user);
        jmsSender.sendMessage(queueName, new UserInfoDto(id, UserAction.DELETE));
    }

    @Transactional
    public boolean isExist(UUID id) {
        if (ObjectUtils.isEmpty(id)) throw new IllegalArgumentException("Id is empty.");
        return userRepo.existsById(id);
    }

    @Async
    void logInfo(String operationType, UserDto userDto) {
        log.info("Operation type: " + operationType + ", result: " + userDto.toString());
    }

    @Transactional(readOnly = true)
    public PageDto<UserDto> getUsers(Search<UserSearchDto> userSearchDto) {
        Page<User> page = userRepo
                .findAll(getSpec(userSearchDto.getData()), getOf(userSearchDto));
        var users = page
                .map(user -> mapperFacade.map(user, UserDto.class)).toList();
        final PageDto<UserDto> pageDto = new PageDto();
        pageDto.setData(users);
        pageDto.setTotal(page.getTotalElements());
        return pageDto;
    }

    @Transactional(readOnly = true)
    public List<User> findAllDraftUsers(){
        final UserSearchDto dto = new UserSearchDto();
        dto.setStatus(UserStatus.DRAFT.toString());
        return userRepo.findAll(getSpec(dto));
    }

    private PageRequest getOf(Search<UserSearchDto> userSearchDto) {
        var page = userSearchDto.getPage();
        return PageRequest.of(page.getPage(), page.getSize());
    }

    private Specification<User> getSpec(UserSearchDto userSearchDto) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (!ObjectUtils.isEmpty(userSearchDto.getUsername())) {
                predicates.add(builder.like(root.get("username"), userSearchDto.getUsername()));
            }
            if (!ObjectUtils.isEmpty(userSearchDto.getStatus())) {
                predicates.add(root.get("status").in(UserStatus.valueOf(userSearchDto.getStatus())));
            }
            return builder.and(predicates.toArray(Predicate[]::new));
        };
    }
}
