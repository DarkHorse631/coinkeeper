package inc.brocorp.service;

import inc.brocorp.model.TokenDto;
import inc.brocorp.model.UserAuthDto;
import org.apache.tomcat.websocket.AuthenticationException;

public interface AuthService {

    TokenDto authenticate(UserAuthDto userAuthDto) throws AuthenticationException;
}
