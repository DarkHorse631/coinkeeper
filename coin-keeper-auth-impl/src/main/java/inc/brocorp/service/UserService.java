package inc.brocorp.service;




import inc.brocorp.model.User;
import inc.brocorp.model.UserDto;
import inc.brocorp.model.UserIdentificationDto;
import inc.brocorp.model.search.PageDto;
import inc.brocorp.model.search.Search;
import inc.brocorp.model.search.UserSearchDto;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.UUID;

/**
 * Класс содержащий API для работы с аккаунтом пользователя
 */
public interface UserService {

    /**
     * Метод для создания аккаунта пользователя
     *
     * @param user - аккаунт, данные которого будут сохранены
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    UserDto create(UserIdentificationDto user);

    /**
     * Метод для поиска аккаунта пользователя по его id
     *
     * @param id - по которому будет происходить поиск
     * @return данные аккаунта пользователя
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    UserDto findById(UUID id);

    /**
     * Поиск пользователя по имени пользователя
     * @param username  - имя пользователя
     * @return данные пользователя с соответствующим именем
     */
    User findByUsername(String username);

    PageDto<UserDto> getUsers(Search<UserSearchDto> userSearchDto);

    /**
     * Метод для обновления данных об аккаунте пользователя
     *
     * @param user - объект, содержащий новые данные пользователя
     * @return обновленные данные
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    UserDto update(UserDto user);

    /**
     * Метод для удаления аккаунта пользователя
     *
     * @param id - по которому необходимо удалить аккаунт
     */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void delete(UUID id);
}
