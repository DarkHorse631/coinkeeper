package inc.brocorp.service;

import inc.brocorp.Bootstrapper;
import inc.brocorp.exception.EntityException;
import inc.brocorp.model.User;
import inc.brocorp.model.UserDto;
import inc.brocorp.model.UserIdentificationDto;
import inc.brocorp.repo.UserRepo;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@ContextConfiguration(classes = Bootstrapper.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Profile("test")
public class UserServiceTest {

    @Autowired
    UserServiceImpl subj;

    @Autowired
    UserRepo userRepo;

    @Autowired
    MapperFacade mapperFacade;

    @Test
    public void createWithEmptyUsername() {
        var user = getUserIdentificationDto();
        user.setUsername(null);
        assertThrows(IllegalArgumentException.class, () -> subj.create(user));
    }

    @Test
    public void createWithWrongPassword() {
        var user = getUserIdentificationDto();
        user.setRepeatPassword("pa");
        assertThrows(RuntimeException.class, () -> subj.create(user));
    }

    @Test
    public void createTest() {
        var user = getUserIdentificationDto();
        when(mapperFacade.map(any(UserIdentificationDto.class), eq(User.class))).thenReturn(getUser());
        when(mapperFacade.map(any(), eq(UserDto.class))).thenReturn(getUserDto());
        var result = subj.create(user);

        assertNotNull(result);
        verify(mapperFacade, times(2)).map(any(), any());
        verify(userRepo, times(1)).save(any());
    }

    @Test
    public void findByEmptyId() {
        assertThrows(IllegalArgumentException.class, () -> subj.findById(null));
    }

    @Test
    public void notFoundById() {
        assertThrows(EntityException.class, () -> subj.findById(UUID.randomUUID()));
    }

    @Test
    public void findByIdTest() {
        UUID id = UUID.randomUUID();
        when(userRepo.existsById(id)).thenReturn(true);
        Optional<User> userOptional = Optional.of(new User());
        when(userRepo.findById(any())).thenReturn(userOptional);
        when(mapperFacade.map(any(), eq(UserDto.class))).thenReturn(getUserDto());
        var result = subj.findById(id);

        assertNotNull(result);
        verify(userRepo, times(1)).existsById(id);
        verify(userRepo, times(1)).findById(id);
    }

    @Test
    public void findBYEmptyName() {
        assertThrows(IllegalArgumentException.class, () -> subj.findByUsername(""));
    }

    @Test
    public void findByUsernameTest() {
        String username = "user";
        when(userRepo.findByUsername(username)).thenReturn(getUser());
        var result = subj.findByUsername(username);
        assertNotNull(result);
        verify(userRepo, times(1)).findByUsername(username);
    }

    @Test
    public void updateWithEmptyId() {
        assertThrows(IllegalArgumentException.class, () -> subj.update(new UserDto()));
    }

    @Test
    public void updateTest() {
        var user = getUserDto();
        when(userRepo.existsById(user.getId())).thenReturn(true);
        when(mapperFacade.map(eq(user), eq(User.class))).thenReturn(getUser());
        when(mapperFacade.map(any(), eq(UserDto.class))).thenReturn(getUserDto());

        var result = subj.update(user);

        assertNotNull(result);
        verify(userRepo, times(1)).save(any());
        verify(mapperFacade, times(2)).map(any(), any());
    }

    @Test
    public void deleteButNotFound(){
        assertThrows(EntityException.class, ()-> subj.delete(UUID.randomUUID()));
    }

    @Test
    public void deleteTest(){
        UUID id = UUID.randomUUID();
        when(userRepo.existsById(id)).thenReturn(true);
        Optional<User> userOptional = Optional.of(new User());
        when(userRepo.findById(any())).thenReturn(userOptional);

        subj.delete(id);

        verify(userRepo, times(1)).save(any());
    }


    UserDto getUserDto() {
        var user = new UserDto();
        user.setEmail("email");
        user.setId(UUID.randomUUID());
        user.setUsername("user");
        return user;
    }

    UserIdentificationDto getUserIdentificationDto() {
        var user = new UserIdentificationDto();
        user.setUsername("user");
        user.setPassword("pass");
        user.setRepeatPassword("pass");
        user.setEmail("mail.ru");
        return user;
    }

    User getUser() {
        var user = new User();
        user.setUsername("user");
        user.setPassword("pass");
        return user;
    }
}
