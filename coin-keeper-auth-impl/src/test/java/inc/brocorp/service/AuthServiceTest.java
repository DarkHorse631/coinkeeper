package inc.brocorp.service;

import inc.brocorp.Bootstrapper;
import inc.brocorp.enums.Role;
import inc.brocorp.exception.AuthenticationException;
import inc.brocorp.model.TokenDto;
import inc.brocorp.model.User;
import inc.brocorp.model.UserAuthDto;
import inc.brocorp.repo.UserRepo;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@ExtendWith({MockitoExtension.class, SpringExtension.class})
@ContextConfiguration(classes = Bootstrapper.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Profile("test")
public class AuthServiceTest {

    @Autowired
    AuthServiceImpl subj;

    @Autowired
    UserService userService;

    @Autowired
    UserRepo userRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MapperFacade mapperFacade;

    @Test
    public void authenticateWithWrongData(){
        var user = getUserAuthDto();
        user.setUsername(null);
        assertThrows(IllegalArgumentException.class, ()-> subj.authenticate(user));
    }

    @Test
    public void authenticateWithWrongPassword(){
        var user = getUserAuthDto();
        user.setUsername("user1");
        when(userService.findByUsername(user.getUsername())).thenReturn(getUser());
        assertThrows(AuthenticationException.class, ()-> subj.authenticate(user));
    }

    @Test
    public void authenticateTest(){
        var user = getUserAuthDto();
        user.setUsername("user1");

        when(userService.findByUsername(user.getUsername())).thenReturn(getUser());
        when(passwordEncoder.matches(any(), any())).thenReturn(true);

        TokenDto tokenDto = subj.authenticate(user);

        assertNotNull(tokenDto.getToken());

        verify(passwordEncoder, times(1)).matches(any(), any());
        verify(userRepo, times(1)).findByUsername(anyString());
    }


    public UserAuthDto getUserAuthDto() {
        var userAuthDto = new UserAuthDto();
        userAuthDto.setUsername("user");
        userAuthDto.setPassword("pass");
        userAuthDto.setAudience("Coin-keeper");
        return userAuthDto;
    }

    public User getUser(){
        var user = new User();
        user.setPassword("pass1");
        user.setUsername("user1");
        user.setEmail("mail@mail.ru");
        user.setRole(Role.USER);
        return user;
    }
}
