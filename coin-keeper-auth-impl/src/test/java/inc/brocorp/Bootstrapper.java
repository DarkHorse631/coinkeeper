package inc.brocorp;

import inc.brocorp.repo.UserRepo;
import inc.brocorp.service.EventService;
import ma.glasnost.orika.MapperFacade;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.password.PasswordEncoder;

@TestConfiguration
@ComponentScan(basePackages = {"inc.brocorp.service"})
@PropertySource(value = "classpath:application.properties")
public class Bootstrapper {

    @MockBean
    UserRepo userRepo;

    @MockBean
    MapperFacade mapperFacade;

    @MockBean
    PasswordEncoder passwordEncoder;

    @MockBean
    EventService eventService;
}
