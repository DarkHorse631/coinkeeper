package inc.brocorp.enums;

public enum MessageStatus {
    SENT("Отправлено"),
    ERROR("Ошибка"),
    SUCCESS("Успешно доставлено");

    private final String description;

    MessageStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}
