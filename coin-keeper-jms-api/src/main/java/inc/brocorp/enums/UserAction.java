package inc.brocorp.enums;

public enum UserAction {

    CREATE("Пользователь создан"),
    DELETE("Пользователь удален");

    public String getDescription() {
        return description;
    }

    private String description;

    UserAction(String description) {
        this.description = description;

    }

}
