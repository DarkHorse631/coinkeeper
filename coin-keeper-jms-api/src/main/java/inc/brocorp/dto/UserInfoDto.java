package inc.brocorp.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import inc.brocorp.enums.MessageStatus;
import inc.brocorp.enums.UserAction;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Класс описывающий сообщение с информацией о пользователе которого зарегистрировали в системе
 */
public class UserInfoDto implements Serializable {

    //Идентификатор объекта
    private UUID id;

    //Статус сообщения
    private MessageStatus messageStatus;

    //Идентификатор зарегистрированного пользователя
    private UUID userId;

    //Действие которое совершили над пользователем
    private UserAction action;

    //Дата создания сообщения
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime createAt;

    public UserInfoDto() {
    }

    public UserInfoDto(UUID userId, UserAction action) {
        this.userId = userId;
        this.action = action;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public MessageStatus getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(MessageStatus messageStatus) {
        this.messageStatus = messageStatus;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UserAction getAction() {
        return action;
    }

    public String getActionDescription() {
        return action == null ? null : action.getDescription();
    }

    public void setAction(UserAction action) {
        this.action = action;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserInfoDto{");
        sb.append("id=").append(id);
        sb.append(", messageStatus=").append(messageStatus);
        sb.append(", userId=").append(userId);
        sb.append(", action=").append(action);
        sb.append(", createAt=").append(createAt);
        sb.append('}');
        return sb.toString();
    }
}
